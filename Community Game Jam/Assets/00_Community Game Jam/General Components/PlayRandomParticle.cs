﻿using UnityEngine;

namespace CMGJ.Utility
{
    public class PlayRandomParticle : MonoBehaviour
    {
        [SerializeField] ParticleSystem[] systems;
        public virtual void PlayRandomParticleSystem()
        {
            ParticleSystem ps = GetRandomParticle();
            if (ps!=null)
            {
                ps.Play(true);
            }
        }
        protected ParticleSystem GetRandomParticle()
        {
            if (systems.Length == 0)
            {
                Debug.Log("Couldn't Play Random Particle System, systems field is empty");
                return null;
            }
            int systemIndex = Random.Range(0, systems.Length);
            return systems[systemIndex];
        }
    }
}