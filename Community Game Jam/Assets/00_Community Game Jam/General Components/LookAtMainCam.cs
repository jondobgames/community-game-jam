﻿using UnityEngine;

namespace CMGJ.Utility
{
    public class LookAtMainCam : MonoBehaviour
    {
        Transform mainCam;
        [Tooltip("Every number of frames")]
        [SerializeField] bool instant = true;
        [SerializeField] int framesBetweenUpdates = 5;
        [SerializeField] float lookingSpeed = 260.0f;
        int framesCounter = 0;
        Quaternion targetRot;
        void Start()
        {
            mainCam = Camera.main.transform;
        }
        void Update()
        {
            if (instant)
            {
                transform.LookAt(mainCam);
                return;
            }
            if (framesCounter < framesBetweenUpdates)
            {
                framesCounter++;
                return;
            }
            framesCounter = 0;
            targetRot = Quaternion.LookRotation(-mainCam.transform.position + transform.position);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRot, lookingSpeed * Time.deltaTime);
        }
    }
}