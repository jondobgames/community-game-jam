﻿using UnityEngine;

namespace CMGJ.Utility
{
    public class Spinner : MonoBehaviour
    {
        [SerializeField]
        float xRotationPerMinute = 1;
        [SerializeField]
        float yRotationPerMinute = 1;
        [SerializeField]
        float zRotationPerMinute = 1;

        float xPerFrame, yPerFrame, zPerFrame;
        float deltaTime;
        private void LateUpdate()
        {
            deltaTime = Time.deltaTime;
            xPerFrame = MathExtensions.RotPerMinuteToRotPerFrame(xRotationPerMinute, deltaTime);
            yPerFrame = MathExtensions.RotPerMinuteToRotPerFrame(yRotationPerMinute, deltaTime);
            zPerFrame = MathExtensions.RotPerMinuteToRotPerFrame(zRotationPerMinute, deltaTime);

            transform.Rotate(Vector3.right, xPerFrame);
            transform.Rotate(Vector3.up, yPerFrame);
            transform.Rotate(Vector3.forward, zPerFrame);
        }
    }
}