﻿using UnityEngine;

namespace CMGJ.Utility
{
    public class PlayGunRandomParticle : PlayRandomParticle
    {
        [SerializeField] Transform[] gunPoints;
        public override void PlayRandomParticleSystem()
        {
            Transform randomGunPoint = GetRandomGunPoint();
            if (randomGunPoint!=null)
            {
                ParticleSystem ps = GetRandomParticle();
                if (ps==null)
                {
                    return;
                }
                ps.transform.position = randomGunPoint.transform.position;
                ps.transform.rotation = randomGunPoint.transform.rotation;
                ps.Play(true);
            }
        }

        private Transform GetRandomGunPoint()
        {
            if (gunPoints.Length==0)
            {
                return null;
            }
            int gunPointIndex = Random.Range(0, gunPoints.Length);
            return gunPoints[gunPointIndex];
        }
    }
}