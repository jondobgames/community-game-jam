﻿using UnityEngine;

namespace CMGJ
{
    public class Singleton<T> : MonoBehaviour where T : Singleton<T>
    {
        static T instance;
        public static bool Exists { get { return instance != null; } }
        public static T Instance
        {
            get
            {
                SafeSetOfInstance();
                return instance;
            }
        }

        private static void SafeSetOfInstance()
        {
            if (Exists == false)
            {
                GameObject singletonsHolderObj = FindObjectOfType<SingletonsGO>().gameObject;
                instance = singletonsHolderObj.GetComponent<T>();
                if (Exists == false)
                {
                    instance = singletonsHolderObj.AddComponent<T>();
                }
            }
        }
        private void ProtectFromDoubles()
        {
            if (Exists && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                SafeSetOfInstance();
            }
        }

        protected virtual void Awake()
        {
            ProtectFromDoubles();
        }
        private void OnDestroy()
        {
            if (instance==this)
            {
                instance = null;
            }
        }
    }
}