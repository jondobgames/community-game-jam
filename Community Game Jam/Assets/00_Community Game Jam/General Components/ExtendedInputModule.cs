﻿using UnityEngine.EventSystems;

namespace CMGJ.CameraAndUI
{
    public class ExtendedInputModule : StandaloneInputModule
    {
        public PointerEventData GetPointerEventData(int pointerID = -1)
        {
            PointerEventData pEventDat = null;
            GetPointerData(pointerID, out pEventDat, true);
            return pEventDat;
        }
    }
}