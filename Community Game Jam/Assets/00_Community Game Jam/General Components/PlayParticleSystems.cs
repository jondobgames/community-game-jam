﻿using UnityEngine;

namespace CMGJ.Units
{
    public class PlayParticleSystems : MonoBehaviour
    {
        [SerializeField] ParticleSystem[] systems;
        public void PlayAll()
        {
            foreach (ParticleSystem sys in systems)
            {
                sys.Play(true);
            }
        }
        public void StopAll()
        {
            foreach (ParticleSystem sys in systems)
            {
                sys.Stop(true);
            }
        }
    }
}