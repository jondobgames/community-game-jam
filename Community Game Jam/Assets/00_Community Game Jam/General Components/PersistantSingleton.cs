﻿namespace CMGJ
{
    public class PersistantSingleton<T> : Singleton<T> where T : PersistantSingleton<T>
    {
        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }
    }
}