﻿using UnityEngine;

namespace CMGJ.Units
{
    public class ObjectsParenter : MonoBehaviour
    {
        [SerializeField] Transform levelToGoAbove;
        [SerializeField] float timeToDie = 5.0f;
        public void TriggerDeath()
        {
            Destroy(gameObject, timeToDie);
        }
        public void UnParent()
        {
            if (levelToGoAbove==null)
            {
                transform.SetParent(transform.parent.transform.parent);
                return;
            }
            transform.SetParent(levelToGoAbove.parent);
        }
    }
}