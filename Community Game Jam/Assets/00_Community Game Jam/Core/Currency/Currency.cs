﻿using System;
using UnityEngine;
namespace CMGJ
{
    [Serializable]
    public class Currency 
    {
        public float timeBetweenIncreases;
        public float increasePerTick;
        public float currentCurrency;

        float counter;
        public bool CanAfford(float price)
        {
            return currentCurrency >= price;
        }
        public bool Update()
        {
            counter += Time.deltaTime / timeBetweenIncreases;
            if (counter>=1)
            {
                counter = 0;
                currentCurrency = currentCurrency + increasePerTick;
                return true;
            }
            return false;
        }
        public void Add(float currency)
        {
            currentCurrency = currentCurrency + currency;
        }
        public void Subtract(float currency)
        {
            Add(-currency);
        }
    }
}