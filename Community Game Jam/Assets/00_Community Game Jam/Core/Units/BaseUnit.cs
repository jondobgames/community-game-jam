﻿using System;
using UnityEngine;
using UnityEngine.Networking;
namespace CMGJ.Units
{
    public abstract class BaseUnit : NetworkBehaviour
    {
        public int UnitID { get { return unitID; } }
        [Header("BaseUnit Unit Read Only")]
        [SerializeField] int unitID = -1;
        UnitsCollectionManager s__UnitsCollectionManager;
        UnitsKiller s_UnitsKiller;
        UnitsKiller S_UnitsKiller
        {
            get
            {
                if (s_UnitsKiller==null)
                {
                    s_UnitsKiller = FindObjectOfType<UnitsKiller>();
                }
                return s_UnitsKiller;
            }
        }
        
        protected void s_ConnectUnitToEveryone(int unitID)
        {
            if (isServer==false)
            {
                return;
            }
            SetUnitID(unitID);
            if (s__UnitsCollectionManager == null)
            {
                s__UnitsCollectionManager = FindObjectOfType<UnitsCollectionManager>();
            }
            if (s_UnitsKiller==null)
            {
                s_UnitsKiller = FindObjectOfType<UnitsKiller>();
            }
            s__UnitsCollectionManager.AddUnit(this, this.unitID);

            Rpc_SetUnitIDToAllClients(unitID);
        }
        public void ConnectID()
        {
            if (isServer==false)
            {
                return;
            }
            Rpc_SetUnitIDToAllClients(unitID);
        }
        [ClientRpc]
        private void Rpc_SetUnitIDToAllClients(int unitID)
        {
            SetUnitID(unitID);
        }
        protected void SetUnitID(int unitID)
        {
            this.unitID = unitID;
        }
        public void Die()
        {
            if (hasAuthority)
            {
                Cmd_Die();
            }
            else
            {
                Destroy(gameObject);
            }
        }

        [Command]
        private void Cmd_Die()
        {
            S_UnitsKiller.s_KillUnit(gameObject);
        }
    }
}