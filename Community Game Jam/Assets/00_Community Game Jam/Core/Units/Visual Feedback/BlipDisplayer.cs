﻿using UnityEngine;

namespace CMGJ.Units
{
    public class BlipDisplayer : MonoBehaviour
    {
        [SerializeField] SpriteRenderer blipRenderer;
        public void Local_AssignColor(Player p)
        {
            Local_SetAccent(p.AccentColor);
        }
        private void Local_SetAccent(Color c)
        {
            blipRenderer.color = c;
        }
    }
}