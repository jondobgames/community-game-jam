﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace CMGJ.CameraAndUI
{
    public class HealthDisplayer : MonoBehaviour
    {
        [Serializable]
        class PercentToColor
        {
            [Range(0,1)]
            public float percent;
            public Color color;
            public PercentToColor() { }
            public PercentToColor(Color color,float percent)
            {
                this.color = color;
                this.percent = percent;
            }
        }
        [Header("Bar Details")]
        [SerializeField] PercentToColor[] colorLevels;
        [SerializeField] Slider healthBar;
        [SerializeField] float reachColorSpeed = 10.0f;
        [Tooltip("Every number of frames")]
        [SerializeField] int framesBetweenUpdates = 5;
        [Header("Border Details")]
        [SerializeField] Image border;
        [SerializeField] Sprite redBorder;
        [SerializeField] Sprite greenBorder;
        Color targetColor;
        ColorBlock colors;
        float targetValue;
        int framesCounter = 0;
        float lerpCounter = 0;
        public void UpdateHealth(float newHealthAsPercent)
        {
            targetValue = newHealthAsPercent;
            targetColor = GetColorOf(newHealthAsPercent);
        }
        private Color GetColorOf(float newHealthAsPercent)
        {
            Color c = new Color();
            foreach (PercentToColor item in colorLevels)
            {
                if (item.percent<=newHealthAsPercent)
                {
                    c = item.color;
                }
            }
            return c;
        }
        private void Start()
        {
            healthBar.value = healthBar.maxValue;
            targetValue = healthBar.value;
            colors = healthBar.colors;
            targetColor = GetColorOf(healthBar.value);
            colors.normalColor = targetColor;
            healthBar.colors = colors;
        }
        private void Update()
        {
            if (framesCounter<=framesBetweenUpdates)
            {
                framesCounter++;
                lerpCounter += Time.deltaTime * reachColorSpeed;
                return;
            }
            colors.normalColor = Color.Lerp(colors.normalColor, targetColor, lerpCounter);
            healthBar.value = Mathf.Lerp(healthBar.value, targetValue, lerpCounter);
            healthBar.colors = colors;
            framesCounter = 0;
            lerpCounter = 0;
        }
        public void AssignBorder(bool hasAuthority)
        {
            if (hasAuthority)
            {
                border.sprite = greenBorder;
                return;
            }
            border.sprite = redBorder;
        }
    }
}