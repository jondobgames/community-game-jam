﻿using UnityEngine;
using CMGJ.CameraAndUI;
namespace CMGJ.Units
{
    public class Informative : MonoBehaviour, ISelect
    {
        [SerializeField] UnitInfo unitInfo;
        [SerializeField] Health unitHealth;

        PlayerUIManager playerUIManager;
        private void Reset()
        {
            unitHealth = GetComponent<Health>();
        }
        public void Select()
        {
            if (playerUIManager)
            {
                playerUIManager.DisplayUnitInformation(unitInfo, unitHealth.CurrentHealth, unitHealth.TotalHealth);
            }
        }
        public void DeSelect()
        {

        }
        private void Start()
        {
            playerUIManager = FindObjectOfType<PlayerUIManager>();
            if (unitHealth == null)
            {
                unitHealth = GetComponent<Health>();
            }
        }
    }
}