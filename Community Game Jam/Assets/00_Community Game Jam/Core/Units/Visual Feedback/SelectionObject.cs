﻿using UnityEngine;

namespace CMGJ
{
    public class SelectionObject : MonoBehaviour
    {
        const string Die_KEY = "Die";
        public void TriggerDeath()
        {
            if (this!=null&&gameObject!=null)
            {
                GetComponent<Animator>().SetTrigger(Die_KEY);
            }
        }
        private void Destroy()
        {
            Destroy(gameObject);
        }
        public void ApplyTint(Color accentColor)
        {
            GetComponentInChildren<SpriteRenderer>().color = accentColor;
        }
    }
}