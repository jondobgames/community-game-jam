﻿using UnityEngine;

namespace CMGJ.Units
{
    public class AccentDisplayer : MonoBehaviour
    {
        [SerializeField] MeshRenderer[] accentRenderers;
        public void Local_AssignAccent(Player p)
        {
            Local_SetAccent(p.AccentColor);
        }
        private void Local_SetAccent(Color c)
        {
            foreach (MeshRenderer mr in accentRenderers)
            {
                Material m = mr.material;
                m.color = c;
            }
        }
    }
}