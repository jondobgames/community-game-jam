﻿using UnityEngine;

namespace CMGJ.Units
{
    public class SelectionDisplayer : MonoBehaviour, ISelect
    {
        [SerializeField] SelectionObject selectionObj;
        SelectionObject currSelectionObj;
        Player owner;
        public void Select()
        {
            currSelectionObj = Instantiate(selectionObj);
            currSelectionObj.transform.SetParent(transform);
            currSelectionObj.transform.localPosition = Vector3.zero;
            currSelectionObj.ApplyTint(owner.AccentColor);
        }
        public void DeSelect()
        {
            currSelectionObj.TriggerDeath();
        }
        public void Local_AssignSelectionTint(Player owner)
        {
            this.owner = owner;
        }
    }
}