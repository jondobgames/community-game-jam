﻿using UnityEngine.Networking;
using UnityEngine;
using System;
using System.Collections;

namespace CMGJ
{
    public class UnitPositionSyncer : UnitSyncer
    {
        [Header("Position-Sync Specific")]
        [SerializeField] float movementSyncThreshold = 0.05f;
        [SerializeField] float posSnapThreshold = 5.0f;
        
        [Header("Used to keep getting closer to the server details")]
        [SerializeField] float positionCatchUpSpeed = 100.0f;


        [Header("Read Only")]
        [SerializeField] Vector3 serverPosition;

        float sqrSnapThreshold;
        Vector3 displacement;
        float sqrMovementThreshold;

        Vector3 tempNewServerPosition;
        protected override void Start()
        {
            base.Start();
            sqrSnapThreshold = posSnapThreshold * posSnapThreshold;
            sqrMovementThreshold = movementSyncThreshold * movementSyncThreshold;
        }
            
        protected override void GetCloserToServerData()
        {
            displacement = toSync.position - serverPosition;
            if (displacement.sqrMagnitude > sqrSnapThreshold)
            {
                if (syncLocal)
                {
                    toSync.localPosition = serverPosition;
                    return;
                }
                toSync.position = serverPosition;
                return;
            }
            if (syncLocal)
            {
                toSync.localPosition = Vector3.MoveTowards(toSync.localPosition, serverPosition, positionCatchUpSpeed * Time.deltaTime);
                return;
            }
            toSync.position = Vector3.MoveTowards(toSync.position, serverPosition, positionCatchUpSpeed * Time.deltaTime);
        }
        protected override bool CanSendDataToServer(bool forceUpdate)
        {
            displacement = GetPosition() - serverPosition;
            return displacement.sqrMagnitude > sqrMovementThreshold || forceUpdate;
        }
        protected override void AuthorityUpdate()
        {
            tempNewServerPosition = GetPosition();
            if (serverPosition == tempNewServerPosition)
            {
                return;
            }
            Cmd_SendDataToServer(tempNewServerPosition);
        }
        [Command]
        private void Cmd_SendDataToServer(Vector3 newServerPosition)
        {
            //Debug.Log("Sending New Server Position_" + serverPosition);
            serverPosition = newServerPosition;
            Rpc_UpdateServerPosOnClients(serverPosition);
        }
        [ClientRpc]
        private void Rpc_UpdateServerPosOnClients(Vector3 newServerPosition)
        {
            serverPosition = newServerPosition;
            //Debug.Log(name + "_has recieved position of_" + serverPosition);
        }

        private Vector3 GetPosition()
        {
            Vector3 newServerPos = new Vector3();
            if (syncLocal)
            {
                newServerPos = toSync.localPosition;
            }
            else
            {
                newServerPos = toSync.position;
            }
            return newServerPos;
        }

        protected override void ForcedUnrestrictedSync()
        {
            tempNewServerPosition = GetPosition();
            Cmd_SendDataToServer(tempNewServerPosition);
        }
    }
}