﻿namespace CMGJ.Units
{
    public class PlayerDetectable : Detectable
    {
        public Unit Unit { get { return BaseUnit as Unit; } }
    }
}