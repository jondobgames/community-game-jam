﻿using CMGJ.AI;

namespace CMGJ.Units
{
    
    public class BiUnitsScanner : UnitsScanner
    {
        AiScanner aiScanner;
        PlayerScanner playerScanner;
        private void Reset()
        {
            gameObject.AddComponent<AiScanner>();
            gameObject.AddComponent<PlayerScanner>();
        }
        protected override void Start()
        {
            base.Start();
            AiScanner.IsOff();
            PlayerScanner.IsOff();
        }
        public AiScanner AiScanner
        {
            get
            {
                if (aiScanner==null)
                {
                    aiScanner = GetComponent<AiScanner>();
                }
                return aiScanner;
            }
        }

        public PlayerScanner PlayerScanner
        {
            get
            {
                if (playerScanner==null)
                {
                    playerScanner=  GetComponent<PlayerScanner>();
                }
                return playerScanner;
            }
        }
        public override bool CanAdd(Detectable detectable)
        {
            return AiScanner.CanAdd(detectable) || PlayerScanner.CanAdd(detectable);
        }
        public override bool CanRemove(Detectable detectable)
        {
            return AiScanner.CanRemove(detectable) || PlayerScanner.CanRemove(detectable);
        }
        public override void OverRideProfile(ScannerProfile scannerProfile)
        {
            base.OverRideProfile(scannerProfile);
            AiScanner.OverRideProfile(scannerProfile);
            PlayerScanner.OverRideProfile(scannerProfile);
        }
    }
}