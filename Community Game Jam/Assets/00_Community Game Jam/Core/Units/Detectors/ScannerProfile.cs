﻿using UnityEngine;

namespace CMGJ.Units
{
    [CreateAssetMenu(menuName = "CMG/Scanner Profile", fileName = "ScannerProfile")]
    public class ScannerProfile : ScriptableObject
    {
        [SerializeField] float detectionRadious = 5;
        public float DetectionRadious { get { return detectionRadious; } }
    }
}