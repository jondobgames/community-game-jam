﻿using UnityEngine;

namespace CMGJ.Units
{
    public abstract class Detectable : MonoBehaviour
    {
        [SerializeField] BaseUnit baseUnit;
        public BaseUnit BaseUnit { get { return baseUnit; } }

        private void Reset()
        {
            baseUnit = transform.parent.GetComponent<Unit>();
        }
    }
}