﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CMGJ.Units
{
    public abstract class UnitsScanner : MonoBehaviour
    {
        public ScannerProfile ScannerProfile { get { return scannerProfile; } }

        [SerializeField] ScannerProfile scannerProfile;
        public event Action<BaseUnit> OnUpdatedClosestUnit;
        [Tooltip("if false, then updating the cloest unit" +
            " must be called from outside of this script")]
        [SerializeField] bool autoUpdate;
        [Header("Scanner::Read Only")]
        [SerializeField] BaseUnit closestUnit;
        [SerializeField] List<BaseUnit> unitsInRange = new List<BaseUnit>();
        

        bool didSet = false;
        bool doesTrigger = true;
        public abstract bool CanAdd(Detectable detectable);
        public abstract bool CanRemove(Detectable detectable);

        float lifeTimeCounter;
        protected virtual void Start()
        {
            if (didSet==false)
            {
                OverRideProfile(scannerProfile);
            }
        }
        public void IsOff()
        {
            doesTrigger = false;
        }
        
        public virtual void OverRideProfile(ScannerProfile scannerProfile)
        {
            if (scannerProfile==null)
            {
                return;
            }
            GetComponent<SphereCollider>().radius = scannerProfile.DetectionRadious;
            this.scannerProfile = scannerProfile;
            didSet = true;
        }
        private void OnTriggerEnter(Collider other)
        {
            if (doesTrigger == false)
            {
                return;
            }
            Detectable successDetectable;
            Detectable[] detectables = other.GetComponents<Detectable>();
            if (detectables.Length == 0)
            {
                return;
            }
            if (CanAdd(detectables, out successDetectable) && unitsInRange.Contains(successDetectable.BaseUnit) == false)
            {
                unitsInRange.Add(detectables[0].BaseUnit);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (doesTrigger == false)
            {
                return;
            }
            Detectable successDetectable;
            Detectable[] detectables = other.GetComponents<Detectable>();
            if (detectables.Length == 0)
            {
                return;
            }
            if (CanRemove(detectables, out successDetectable) && unitsInRange.Contains(successDetectable.BaseUnit))
            {
                unitsInRange.Remove(detectables[0].BaseUnit);
            }
        }
        private bool CanAdd(Detectable[] detectables, out Detectable successDetectable)
        {
            foreach (Detectable d in detectables)
            {
                if (CanAdd(d))
                {
                    successDetectable = d;
                    return true;
                }
            }
            successDetectable = null;
            return false;
        }
        private bool CanRemove(Detectable[] detectables,out Detectable successDetectable)
        {
            foreach (Detectable d in detectables)
            {
                if (CanRemove(d))
                {
                    successDetectable = d;
                    return true;
                }
            }
            successDetectable = null;
            return false;
        }
        
        private void Update()
        {
            if (autoUpdate==false)
            {
                return;
            }
            UpdateClosestUnit();
        }
        public void ProtectUnitsInRangeList()
        {
            unitsInRange.RemoveAll(IsUnitNull);
        }
        private bool IsUnitNull(BaseUnit testUnit)
        {
            return testUnit == null;
        }
        /// <summary>
        /// if this is called from outside, the autoUpdate will be forced to false
        /// </summary>
        public void UpdateClosestUnit()
        {
            autoUpdate = false;
            ProtectUnitsInRangeList();
            BaseUnit testClosest = null;
            float smallestSqrDist = float.MaxValue;
            for (int i = 0; i < unitsInRange.Count; i++)
            {
                BaseUnit u1 = unitsInRange[i];
                Vector3 displacement = u1.transform.position - transform.position;
                float sqrDist = displacement.sqrMagnitude;
                if (sqrDist < smallestSqrDist)
                {
                    smallestSqrDist = sqrDist;
                    testClosest = u1;
                }
            }
            if (closestUnit != testClosest)
            {
                closestUnit = testClosest;
                if (OnUpdatedClosestUnit != null)
                {
                    OnUpdatedClosestUnit(testClosest);
                }
            }
        }
    }
}