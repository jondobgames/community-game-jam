﻿using CMGJ.Units;

namespace CMGJ.AI
{
    public class AiScanner : UnitsScanner
    {
        public override bool CanAdd(Detectable detectable)
        {
            return detectable as Ai_Detectable;
        }
        public override bool CanRemove(Detectable detectable)
        {
            return detectable as Ai_Detectable;
        }
    }
}