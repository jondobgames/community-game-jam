﻿namespace CMGJ.Units
{
    public class PlayerScanner : UnitsScanner
    {
        Player sampledPlayer;
        PlayerManagersHelper playerManagersHelper;
        protected override void Start()
        {
            base.Start();
            playerManagersHelper = FindObjectOfType<PlayerManagersHelper>();
            sampledPlayer = new Player();
        }
        public override bool CanAdd(Detectable detectable)
        {
            return CanScan(detectable);
        }
        public override bool CanRemove(Detectable detectable)
        {
            return CanScan(detectable);
        }

        private bool CanScan(Detectable detectable)
        {
            PlayerDetectable pDetectable = detectable as PlayerDetectable;
            if (pDetectable == null)
            {
                return false;
            }
            if (pDetectable.Unit == null)
            {
                return true;
            }
            if (pDetectable.Unit.Owner == sampledPlayer.ID)
            {
                //scanned unit still does not have its player, meaning there is a 99% chance, 
                //its either disconnected, or newly spawned for the same palyer so no need to attack it(scan it)

                //Debug.LogWarning("Can Not Scan " + pDetectable.transform.parent.name + " because the unit's player has not been set yet" +
                //    " therefor it won't be scanned");
                return false;
            }
            return playerManagersHelper.Owner.ID != pDetectable.Unit.Owner;
        }
    }
}