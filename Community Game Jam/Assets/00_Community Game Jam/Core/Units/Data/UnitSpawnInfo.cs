﻿using UnityEngine;

namespace CMGJ.Units
{
    [CreateAssetMenu(fileName ="Unit Spawn Info", menuName = "CMG/Units/Unit Spawn Profile")]
    public class UnitSpawnInfo : ScriptableObject
    {
        public float BuildTime { get { return buildTime; } }
        public float Cost { get { return cost; } }
        public Sprite Icon { get { return icon; } }
        public GameObject UnitPrefab { get { return unitPrefab; } }
        public int UnitID { get { return unitID; } }

        [SerializeField] float buildTime = 1;
        [SerializeField] float cost = 1;
        [SerializeField] Sprite icon = null;
        [SerializeField] GameObject unitPrefab;
        [SerializeField] int unitID;
    }
}