﻿using UnityEngine;

namespace CMGJ.Units
{
    [CreateAssetMenu(menuName = "CMG/Units/Movement Profile", fileName = "Movement Profile")]
    public class UnitMovementProfile : ScriptableObject
    {
        public float TurnSpeed { get { return turnSpeed; } }
        public float MovementSpeed { get { return movementSpeed; } }
        public float StoppingDistance { get { return stoppingDistance; } }
        public float AnimationSpeedMultiplayer { get { return animationSpeedMultiplayer; } }
        public bool UseAnimMovement { get { return useAnimMovement; } }

        [Header("Movement")]
        [SerializeField] bool useAnimMovement = false;
        [SerializeField] float turnSpeed = 280.0f;
        [SerializeField] float movementSpeed = 2.0f;
        [SerializeField] float stoppingDistance = 0.5f;
        [Header("animation movement")]
        [SerializeField] float animationSpeedMultiplayer = 1.0f;
    }
}