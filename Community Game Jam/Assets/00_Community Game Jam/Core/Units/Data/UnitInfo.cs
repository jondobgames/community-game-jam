﻿using UnityEngine;

namespace CMGJ.Units
{
    [CreateAssetMenu(menuName = "CMG/Units/Unit Info", fileName = "New Unit Info")]
    public class UnitInfo : ScriptableObject
    {
        public string UnitName { get { return unitName; } }
        public string UnitDescription { get { return unitDescription; } }
        public Sprite UnitIcon { get { return unitIcon; } }

        [SerializeField] string unitName;
        [TextArea(1,3)] [SerializeField] string unitDescription;
        [SerializeField] Sprite unitIcon;
    }
}