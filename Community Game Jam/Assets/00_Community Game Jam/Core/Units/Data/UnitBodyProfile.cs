﻿using UnityEngine;

namespace CMGJ.Units
{
    [CreateAssetMenu(menuName = "CMG/Units/Unit Body Profile", fileName = "Unit Body Profile")]
    public class UnitBodyProfile : ScriptableObject
    {
        public float TotalHealth { get { return totalHealth; } }
        public float RewardOnDeath { get { return rewardOnDeath; } }

        [SerializeField] float totalHealth = 1;
        [SerializeField] float rewardOnDeath = 10;
    }
}