﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace CMGJ
{
    public abstract class UnitSyncer : NetworkBehaviour
    {
        [Header("Core")]
        [SerializeField] bool usesStartForcedSync = false;
        [SerializeField] float timeBeforeForcedSyncs = 3.0f;
        [SerializeField] protected bool syncLocal = false; 
        [SerializeField] protected Transform toSync;

        [Header("Limits")]
        [SerializeField]float timeBetweenForcfulUpdate = 0.1f; 
        [SerializeField] float timeBetweenUpdates = 0.05f;

        bool forceUpdate;
        float updatesCounter = 0;
        float forcefulUpdateCounter = 0;
        protected virtual void Start()
        {
        }
        public override void OnStartServer()
        {
            base.OnStartServer();
            if (usesStartForcedSync)
            {
                StartCoroutine(ForcedUnRestrictedSend(timeBeforeForcedSyncs));
            }
        }

        private IEnumerator ForcedUnRestrictedSend(float timeBeforeForcedSync)
        {
            yield return new WaitForSeconds(timeBeforeForcedSync);
            ForcedUnrestrictedSync();
        }

        private void Reset()
        {
            toSync = transform;
        }
        private void Update()
        {
            if (hasAuthority)
            {
                forcefulUpdateCounter += Time.deltaTime / timeBetweenForcfulUpdate;
                updatesCounter += Time.deltaTime / timeBetweenUpdates;
                forceUpdate = forcefulUpdateCounter >= 1;
                if (updatesCounter >= 1 || forceUpdate)
                {
                    updatesCounter = 0;
                    if (CanSendDataToServer(forceUpdate))
                    {
                        AuthorityUpdate();
                    }
                    if (forceUpdate)
                    {
                        forceUpdate = false;
                        forcefulUpdateCounter = 0;
                    }
                }
            }
            else
            {
                GetCloserToServerData();
            }
        }
        protected abstract void GetCloserToServerData();
        protected abstract bool CanSendDataToServer(bool forceUpdate);
        protected abstract void AuthorityUpdate();
        protected abstract void ForcedUnrestrictedSync();
        
    }
}