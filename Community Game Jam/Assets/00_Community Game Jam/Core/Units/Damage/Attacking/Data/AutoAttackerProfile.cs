﻿using UnityEngine;

namespace CMGJ.Units
{
    [CreateAssetMenu(menuName = "CMG/Attackers Data/AutoAttacker Profile", fileName = "AutoAttacker Profile")] 
    public class AutoAttackerProfile : ScriptableObject
    {
        public float TurnSpeed { get { return turnSpeed; } }
        public float AngleLimit { get { return angleLimit; } }

        [SerializeField] float turnSpeed = 260.0f;
        [SerializeField] float angleLimit = 5;
    }
}