﻿using UnityEngine;

namespace CMGJ.Units
{
    [CreateAssetMenu(menuName = "CMG/Attackers Data/AttackInfo", fileName = "AttackInfo")]
    public class AttackerInfo : ScriptableObject
    {
        public float TimeBetweenUpdates { get { return timeBetweenUpdates; } }
        public float TimeBetweenShots { get { return timeBetweenShots; } }
        public float Damage { get { return damage; } }
        public float Range { get { return range; } }

        [Header("Core")]
        [SerializeField] float damage = 1;
        [SerializeField] float timeBetweenShots = 1;
        [SerializeField] float range = 3.0f;
        [SerializeField] float timeBetweenUpdates = 0.1f;
    }
}