﻿using UnityEngine;

namespace CMGJ.Units
{
    public class AutoAttacker : Attacker
    {
        public ScannerProfile ScannerProfile { get { return scannerProfile; } }
        public Transform RotatablePart { get { return rotatablePart; } }
        public AutoAttackerProfile AutoAttackerProfile { get { return autoAttackerProfile; } }

        [SerializeField] UnitsScanner unitsScanner;
        [SerializeField] Transform rotatablePart;
        [SerializeField] ScannerProfile scannerProfile;
        [SerializeField] AutoAttackerProfile autoAttackerProfile;
        [SerializeField] bool overRideProfileForScanners = true;

        [Header("Read Only::AutoAttacker")]
        public BaseUnit closestUnit;
        public AutoAttacker_Stopped stopped;
        public AutoAttacker_Kiting kiting;
        public AutoAttacker_FocusedFire focusedFire;

        public AutoAttacker_Idle idle;

        protected override void Start()
        {
            base.Start();

            stopped = new AutoAttacker_Stopped(attackerInfo,  this);
            kiting = new AutoAttacker_Kiting(attackerInfo, this);
            focusedFire = new AutoAttacker_FocusedFire(attackerInfo, this);
            idle = new AutoAttacker_Idle(attackerInfo, this);

            SetUpScannerWithProfile();
            TransitionToState(idle);
        }

        private void SetUpScannerWithProfile()
        {
            if (overRideProfileForScanners && scannerProfile != null)
            {
                unitsScanner.OverRideProfile(scannerProfile);
            }
            else if (scannerProfile == null)
            {
                scannerProfile = unitsScanner.ScannerProfile;
            }

            unitsScanner.OnUpdatedClosestUnit += UnitsDetector_OnUpdatedClosestUnit;
        }
        public void ClearClosestUnit()
        {
            unitsScanner.ProtectUnitsInRangeList();
        }
        private void UnitsDetector_OnUpdatedClosestUnit(BaseUnit updatedClosestUnit)
        {
            closestUnit = updatedClosestUnit;
            if (currState == focusedFire)
            {
                return;
            }
            TransitionToState(kiting, closestUnit, Vector3.zero);
        }
        public override void Charge(BaseUnit unit, Vector3 formationVec)
        {
            TransitionToState(focusedFire, unit, formationVec);
        }
        public override AttackerState IdleState()
        {
            return stopped;
        }
        protected override void TrimmedUpdate()
        {
            base.TrimmedUpdate();

            unitsScanner.UpdateClosestUnit();
        }
    }
}