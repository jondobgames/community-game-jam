﻿using UnityEngine;
using System;
using UnityEngine.Networking;
using UnityEngine.AI;

namespace CMGJ.Units
{
    public abstract class Attacker : NetworkBehaviour
    {
        public void TransitionToState(AttackerState newState)
        {
            if (newState != null && newState != currState)
            {
                if (currState != null)
                {
                    currState.OnExit();
                }
                newState.OnEnter();
                currState = newState;
            }
        }

        public event Action OnAttackTriggered;
        public event Action OnAttackEnded;

        /// <summary>
        /// ServerVariable
        /// </summary>
        UnitsCollectionManager s_UnitsCollectionManager;
        BaseUnit me;

        protected AttackerState currState;
        protected UnitMovement unitMovement;
        protected NavMeshAgent agent;

        [SerializeField] protected AttackerInfo attackerInfo;
        [SerializeField] ToggleEvent onAttackTriggered;

        /// <summary>
        /// the ai won't be needing a formation, so it will usually use this overload
        /// </summary>
        /// <param name="unit"></param>
        public void Charge(BaseUnit unit)
        {
            Charge(unit, Vector3.zero);
        }
        /// <summary>
        /// called to start the entire attacking process (can be called from selection Manager);
        /// </summary>
        /// <param name="unit"></param>
        /// <param name="formationVec"></param>
        public abstract void Charge(BaseUnit unit, Vector3 formationVec);
        public abstract AttackerState IdleState();

        float updateCounter = 0;
        float nextAttackCounter = 0;

        public void TryAttacking(BaseUnit unitToDamage)
        {
            if (nextAttackCounter < 1)
            {
                return;
            }
            nextAttackCounter = 0;
            ServerAttackTriggered();
            Cmd_Strike(unitToDamage.UnitID);
        }
        private void Update()
        {
            if (hasAuthority == false || currState == null)
            {
                return;
            }
            if (nextAttackCounter<1)
            {
                nextAttackCounter += Time.deltaTime / attackerInfo.TimeBetweenShots;
            }
            currState.Update();
            PermanentUpdate();
            updateCounter += Time.deltaTime / attackerInfo.TimeBetweenUpdates;
            if (updateCounter<1)
            {
                return;
            }
            updateCounter = 0;
            TrimmedUpdate();
        }
        public void TransitionToState(AttackerState newState, BaseUnit unit, Vector3 formationVec)
        {
            TransitionToState(newState);
            currState.SetTarget(unit, formationVec);
        }
        public virtual void ClearFocus()
        {
            TransitionToState(IdleState(), null, Vector3.zero);
        }
        protected virtual void TrimmedUpdate() { }
        protected virtual void PermanentUpdate() { }

        protected virtual void Start()
        {
            me = GetComponent<BaseUnit>();
            agent = GetComponent<NavMeshAgent>();
            unitMovement = GetComponent<UnitMovement>();
        }

        [Command]
        private void Cmd_Strike(int unitID)
        {
            if (s_UnitsCollectionManager==null)
            {
                s_UnitsCollectionManager = FindObjectOfType<UnitsCollectionManager>();
            }
            if (me==null)
            {
                me = GetComponent<BaseUnit>();
            }
            BaseUnit baseOfUnit = s_UnitsCollectionManager.GetUnitOf(unitID);
            if (baseOfUnit == null)
            {
                return;
            }
            Health healthToHurt = baseOfUnit.GetComponent<Health>();
            if (healthToHurt==null)
            {
                return;
            }

            Unit meU = me as Unit;
            if (meU!=null)
            {
                //used when player attacking some unit
                healthToHurt.TakeDamageOnAllClients(attackerInfo.Damage, meU.Owner);
            }
            else
            {
                //used for AI attacking players
                healthToHurt.TakeDamageOnAllClients(attackerInfo.Damage);
            }
        }
        #region NetworkEvents
        private void ServerAttackTriggered()
        {
            Cmd_AttackTriggered();
        }
        public void ServerAttackEnded()
        {
            Cmd_AttackEnded();
        }
        [Command]
        private void Cmd_AttackTriggered()
        {
            Rpc_AttackTriggered();
        }
        [ClientRpc]
        private void Rpc_AttackTriggered()
        {
            if (onAttackTriggered!=null)
            {
                onAttackTriggered.Invoke(true);
            }
            if (OnAttackTriggered != null)
            {
                OnAttackTriggered();
            }
        }
        [Command]
        private void Cmd_AttackEnded()
        {
            Rpc_AttackEnded();
        }
        [ClientRpc]
        private void Rpc_AttackEnded()
        {
            if (OnAttackEnded != null)
            {
                OnAttackEnded();
            }
        }
        #endregion
    }
}