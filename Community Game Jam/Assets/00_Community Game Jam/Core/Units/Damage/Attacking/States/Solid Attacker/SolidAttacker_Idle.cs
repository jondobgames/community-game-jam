﻿namespace CMGJ.Units
{
    public class SolidAttacker_Idle : SolidAttackerState
    {
        float agentOriginalStopping;
        public SolidAttacker_Idle(AttackerInfo attackerInfo, SolidAttacker attacker) : base(attackerInfo, attacker)
        {
            solidAttacker = attacker;
            agentOriginalStopping = agent.stoppingDistance;
        }
        public override void OnEnter()
        {
            agent.stoppingDistance = agentOriginalStopping;
            unitMovement.Move(agent.transform.position);
        }
        public override void OnExit()
        {
        }
        public override void Update()
        {
        }
    }
}