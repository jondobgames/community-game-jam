﻿using UnityEngine;

namespace CMGJ.Units
{
    public class AutoAttacker_Kiting : AutoAttackerState
    {
        float angleLimit;
        float turnSpeed;
        public AutoAttacker_Kiting(AttackerInfo attackerInfo, AutoAttacker attacker) 
            : base(attackerInfo, attacker)
        {
            turnSpeed = autoAttacker.AutoAttackerProfile.TurnSpeed;
            angleLimit = autoAttacker.AutoAttackerProfile.AngleLimit;
        }
        public override void OnEnter()
        {
            
        }
        public override void OnExit()
        {
            
        }
        public override void Update()
        {
            TurnHeadTowardsClosest();
        }

        private void TurnHeadTowardsClosest()
        {
            if (autoAttacker.closestUnit==null)
            {
                autoAttacker.ClearClosestUnit();
                return;
            }
            Vector3 dir = -autoAttacker.closestUnit.transform.position + agent.transform.position;

            Quaternion targetRot = Quaternion.LookRotation(dir);
            autoAttacker.RotatablePart.rotation =
                Quaternion.RotateTowards(autoAttacker.RotatablePart.rotation,targetRot,turnSpeed*Time.deltaTime);
            
            float angle = Quaternion.Angle(autoAttacker.RotatablePart.rotation, targetRot);
            if (angle<=angleLimit)
            {
                autoAttacker.TryAttacking(autoAttacker.closestUnit);
            }
        }

    }
}