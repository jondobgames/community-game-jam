﻿namespace CMGJ.Units
{
    public class AutoAttacker_Stopped : AutoAttackerState
    {
        float originalStoppingDist;
        public AutoAttacker_Stopped(AttackerInfo attackerInfo,  AutoAttacker attacker) 
            : base(attackerInfo, attacker)
        {
            originalStoppingDist = agent.stoppingDistance;
        }
        public override void OnEnter() 
        {
            agent.stoppingDistance = originalStoppingDist;
            unitMovement.Move(autoAttacker.transform.position);
        }
        public override void OnExit()
        {
        }
        public override void Update()
        {
            if (autoAttacker.closestUnit!=null)
            {
                autoAttacker.TransitionToState(autoAttacker.kiting);
            }
        }
    }
}