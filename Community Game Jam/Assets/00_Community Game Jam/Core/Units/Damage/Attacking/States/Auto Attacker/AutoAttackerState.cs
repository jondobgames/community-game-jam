﻿namespace CMGJ.Units
{
    public abstract class AutoAttackerState : AttackerState
    {
        protected AutoAttacker autoAttacker;
        public AutoAttackerState(AttackerInfo attackerInfo, AutoAttacker attacker)
            : base(attackerInfo, attacker)
        {
            autoAttacker = attacker;
        }
    }
}