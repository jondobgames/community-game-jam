﻿using UnityEngine;

namespace CMGJ.Units
{
    public class SolidAttacker_Attacking : SolidAttackerState
    {
        public SolidAttacker_Attacking(AttackerInfo attackerInfo, SolidAttacker attacker)
            : base(attackerInfo, attacker)
        {
            solidAttacker = attacker;
        } 
        Vector3 lookDir;
        float sqrDist;
        float sqrStoppingDist;
        Vector3 CurrTargetPos
        {
            get
            {
                return unitToAttack.transform.position;
            }
        }
        public override void OnEnter()
        {
            sqrStoppingDist = agent.stoppingDistance * agent.stoppingDistance;
        }

        private void TryAttacking()
        {
            if (unitToAttack==null)
            {
                solidAttacker.TransitionToState(solidAttacker.idle, null, Vector3.zero);
                return;
            }
            solidAttacker.TryAttacking(unitToAttack);
        }
        public override void OnExit()
        {
        }
        public override void Update()
        {
            if (unitToAttack == null)
            {
                solidAttacker.TransitionToState(solidAttacker.idle, null, Vector3.zero);
                return;
            }
            Vector3 posCheck = agent.transform.position - CurrTargetPos;
            sqrDist = (posCheck).sqrMagnitude;

            if (sqrDist > sqrStoppingDist)
            {
                solidAttacker.TransitionToState(solidAttacker.chasing, unitToAttack, formationVector);
                return;
            }
            lookDir = unitToAttack.transform.position - agent.transform.position;
            unitMovement.UpdateTurningTo(Quaternion.LookRotation(lookDir));
            TryAttacking();
        }
    }
}