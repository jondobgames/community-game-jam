﻿namespace CMGJ.Units
{

    public class AutoAttacker_Idle : AutoAttackerState
    {
        public AutoAttacker_Idle(AttackerInfo attackerInfo, AutoAttacker attacker) : base(attackerInfo, attacker)
        {
        }

        public override void OnEnter()
        {
            
        }

        public override void OnExit()
        {
            
        }

        public override void Update()
        {
            
        }
    }
}