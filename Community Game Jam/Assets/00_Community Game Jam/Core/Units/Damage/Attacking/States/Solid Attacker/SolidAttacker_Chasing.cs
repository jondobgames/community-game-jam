﻿using UnityEngine;

namespace CMGJ.Units
{
    public class SolidAttacker_Chasing : SolidAttackerState
    {
        public SolidAttacker_Chasing(AttackerInfo attackerInfo, SolidAttacker attacker)
            : base(attackerInfo, attacker)
        {
        }
        Vector3 CurrTargetPos
        {
            get
            {
                return unitToAttack.transform.position;
            }
        }
        Quaternion CurrTargetRot
        {
            get
            {
                return Quaternion.LookRotation(-agent.transform.position + unitToAttack.transform.position);
            }
        }
        bool didSub = false;
        public override void OnEnter() 
        {
            if (didSub == false)
            {
                unitMovement.OnMovementDone += UnitMovement_OnMovementDone;
                didSub = true;
            }
        }
        public override void OnExit()
        {
            if (didSub == true)
            {
                unitMovement.OnMovementDone -= UnitMovement_OnMovementDone;
                didSub = false;
            }
        }
        public override void Update()
        {
            if (unitToAttack==null)
            {
                solidAttacker.TransitionToState(solidAttacker.idle, null, Vector3.zero);
                return;
            }
            unitMovement.Move(CurrTargetPos, CurrTargetRot);
        }
        public override void SetTarget(BaseUnit unit, Vector3 formationVector)
        {
            base.SetTarget(unit, formationVector);
            agent.stoppingDistance = attackerInfo.Range;

            unitMovement.Move(CurrTargetPos, CurrTargetRot);
        }
        private void UnitMovement_OnMovementDone()
        {
            solidAttacker.TransitionToState(solidAttacker.attacking, unitToAttack, formationVector);
        }
    }
}