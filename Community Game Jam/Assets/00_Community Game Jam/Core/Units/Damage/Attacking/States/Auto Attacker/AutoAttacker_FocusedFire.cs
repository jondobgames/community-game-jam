﻿using UnityEngine;

namespace CMGJ.Units
{
    public class AutoAttacker_FocusedFire : AutoAttackerState
    {
        float angleLimit;
        float turnSpeed;
        float sqrStopping;
        public AutoAttacker_FocusedFire(AttackerInfo attackerInfo, AutoAttacker attacker) : base(attackerInfo, attacker)
        {
        }
        public override void OnEnter()
        {
            angleLimit = autoAttacker.AutoAttackerProfile.AngleLimit;
            turnSpeed = autoAttacker.AutoAttackerProfile.TurnSpeed;

            agent.stoppingDistance = autoAttacker.ScannerProfile.DetectionRadious;
            sqrStopping = agent.stoppingDistance * agent.stoppingDistance;
        }
        public override void OnExit()
        {
        }
        public override void Update()
        {
            if (unitToAttack==null)
            {
                attacker.TransitionToState(autoAttacker.stopped);
                return;
            }

            bool isTurnedEnough = TurnTowardsTargetUnit();
            bool isCloseEnough = IsCloseEnough();

            if (isCloseEnough==false)
            {
                unitMovement.Move(unitToAttack.transform.position);
            }

            if (isCloseEnough && isTurnedEnough)
            {
                autoAttacker.TryAttacking(unitToAttack);
            }
        }

        private bool IsCloseEnough()
        {
            Vector3 displacement = agent.transform.position - unitToAttack.transform.position;
            float sqrDist = displacement.sqrMagnitude;
            return sqrDist < sqrStopping;
        }

        private bool TurnTowardsTargetUnit()
        {
            if (unitToAttack== null)
            {
                attacker.TransitionToState(attacker.IdleState());
                return false;
            }
            Vector3 dir = -unitToAttack.transform.position + agent.transform.position;

            Quaternion targetRot = Quaternion.LookRotation(dir);
            autoAttacker.RotatablePart.rotation =
                Quaternion.RotateTowards(autoAttacker.RotatablePart.rotation, targetRot, turnSpeed * Time.deltaTime);

            float angle = Quaternion.Angle(autoAttacker.RotatablePart.rotation, targetRot);
            return (angle <= angleLimit);
        }
    }
}