﻿using UnityEngine;
using UnityEngine.AI;

namespace CMGJ.Units
{
    public abstract class AttackerState : IState
    {
        protected AttackerInfo attackerInfo;
        protected NavMeshAgent agent;
        protected UnitMovement unitMovement;
        protected Attacker attacker;
        public abstract void OnEnter();
        public abstract void OnExit();
        public abstract void Update();

        protected BaseUnit unitToAttack;
        protected Vector3 formationVector;

        public virtual void SetTarget(BaseUnit unit, Vector3 formationVector)
        {
            unitToAttack = unit;
            this.formationVector = formationVector;
        }

        public AttackerState(AttackerInfo attackerInfo, Attacker attacker)
        {
            this.attackerInfo = attackerInfo;
            this.attacker = attacker;
            agent = attacker.GetComponent<NavMeshAgent>();
            unitMovement = attacker.GetComponent<UnitMovement>();
        }
        /// <summary>
        /// should be called from outside
        /// </summary>
        public virtual void PermanentUpdate() { }
        public virtual void TrimmedUpdate() { }
    }
}