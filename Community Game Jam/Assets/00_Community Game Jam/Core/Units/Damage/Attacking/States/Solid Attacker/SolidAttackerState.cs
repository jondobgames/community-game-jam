﻿namespace CMGJ.Units
{
    public abstract class SolidAttackerState : AttackerState
    {
        protected SolidAttacker solidAttacker;
        public SolidAttackerState(AttackerInfo attackerInfo, SolidAttacker attacker) 
            : base(attackerInfo, attacker)
        {
            solidAttacker = attacker;
        }
    }
}