﻿using UnityEngine;

namespace CMGJ.Units
{
    public class SolidAttacker : Attacker
    {
        public SolidAttacker_Idle idle;
        public SolidAttacker_Chasing chasing;
        public SolidAttacker_Attacking attacking;
        
        protected override void Start()
        {
            base.Start();

            idle = new SolidAttacker_Idle(attackerInfo, this);
            chasing = new SolidAttacker_Chasing(attackerInfo, this);
            attacking = new SolidAttacker_Attacking(attackerInfo, this);
        }
        public override void Charge(BaseUnit unit, Vector3 formationVec)
        {
            TransitionToState(chasing, unit, formationVec);
        }
        public override AttackerState IdleState()
        {
            return idle;
        }
    }
}