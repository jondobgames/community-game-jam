﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace CMGJ.Units
{
    public class Health : NetworkBehaviour
    {
        public float TotalHealth { get { return unitBodyProfile.TotalHealth; } }
        public float CurrentHealth { get { return currentHealth; } }

        [SerializeField] UnityEvent onDeathEvent;
        [SerializeField] FloatEvent onDamageRecieved;
        [SerializeField] FloatEvent onGainedHealth;

        [SerializeField] UnitBodyProfile unitBodyProfile;
        [Header("Read Only")]
        [SerializeField] float currentHealth;
        float HealthAsPercent(float health) { return health / TotalHealth; }
        ResourcesDistributor s_ResourcesDistributor;

        private void Start()
        {
            currentHealth = unitBodyProfile.TotalHealth;
            if (isServer)
            {
                s_ResourcesDistributor = FindObjectOfType<ResourcesDistributor>();
            }
        }
        public void TakeDamageOnAllClients(float damage,int from)
        {
            Cmd_TakeDamage(damage,from);
        }
        public void TakeDamageOnAllClients(float damage)
        {
            Cmd_AnynomusTakeDamage(damage);
        }
        public void HealOnAllClients(float extraHealth)
        {
            Cmd_TakeHealth(extraHealth);
        }
        [Command]
        private void Cmd_TakeDamage(float damage, int from)
        {
            float newHealth = currentHealth - damage;
            //process what should happen for the server,
            //consider creating an event here?
            if (newHealth<=0)
            {
                s_ResourcesDistributor.s_GiveMoneyTo(from, unitBodyProfile.RewardOnDeath);
            }
            //update the health for all players
            Rpc_AssignNewHealth(newHealth);
        }
        [Command]
        private void Cmd_AnynomusTakeDamage(float damage)
        {
            float newHealth = currentHealth - damage;
            Rpc_AssignNewHealth(newHealth);
        }
        [Command]
        private void Cmd_TakeHealth(float extraHealth)
        {
            float newHealth = currentHealth + extraHealth;
            Rpc_AssignNewHealth(newHealth);
        }
        [ClientRpc]
        private void Rpc_AssignNewHealth(float newHealth)
        {
            bool shouldDie = newHealth <= 0;
            bool lostHealth = newHealth < currentHealth;
            if (shouldDie)
            {
                onDeathEvent.Invoke();
            }
            else if (lostHealth && shouldDie == false)
            {
                onDamageRecieved.Invoke(HealthAsPercent(newHealth));
            }
            else if (lostHealth == false && shouldDie == false)
            {
                onGainedHealth.Invoke(HealthAsPercent(newHealth));
            }
            currentHealth = newHealth;
        }
    }
}