﻿using CMGJ.Units;

namespace CMGJ.AI
{
    public class PeacefulAi_Attacking : PeacefulAiState
    {
        Unit closestUnit;
        Attacker attacker;
        public PeacefulAi_Attacking(PeacefulAI ai) : base(ai)
        {
            attacker = ai.GetComponent<Attacker>();
        }
        public override void OnEnter()
        {
        }
        public override void OnExit()
        {
            attacker.TransitionToState(attacker.IdleState());
        }
        public override void Update()
        {
            if (closestUnit==null)
            {
                ai.TransitionToState(ai.idling);
            }
        }
        public void UpdateClosest(Unit closestUnit)
        {
            this.closestUnit = closestUnit;
            if (closestUnit == null)
            {
                ai.TransitionToState(ai.idling);
                return;
            }
            
            attacker.Charge(closestUnit);
        }
    }
}