﻿namespace CMGJ.AI
{
    public abstract class PeacefulAiState : State_Ai
    {
        protected PeacefulAI ai;
        public PeacefulAiState(PeacefulAI ai) : base(ai)
        {
            this.ai = ai;
        }
        public virtual void PermanentUpdate()
        {

        }
    }
}