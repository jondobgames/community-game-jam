﻿using CMGJ.Units;

namespace CMGJ.AI
{
    public class PeacefulAI : NeuturalAI
    {
        public PeacefulAi_Idling idling;
        public PeacefulAi_Attacking attacking;

        protected override void Start()
        {
            base.Start();
            if (isServer)
            {
                unitsDetector.OnUpdatedClosestUnit += OnUpdatedClosestUnit;
            }
            idling = new PeacefulAi_Idling(this);
            attacking = new PeacefulAi_Attacking(this);
            
            TransitionToState(idling);
        }

        private void OnUpdatedClosestUnit(BaseUnit closestUnit)
        {
            if (closestUnit==null)
            {
                TransitionToState(idling);
            }
            Unit playerUnit = closestUnit as Unit;
            if (playerUnit==null)
            {
                return;
            }
            TransitionToState(attacking);
            attacking.UpdateClosest(playerUnit);
        }
        protected override void TrimmedUpdate()
        {
            base.TrimmedUpdate();

            unitsDetector.UpdateClosestUnit();
        }
    }
}