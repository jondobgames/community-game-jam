﻿using CMGJ.Units;
using UnityEngine;
using UnityEngine.AI;

namespace CMGJ.AI
{
    public abstract class NeuturalAI : BaseUnit
    {
        [SerializeField] protected AIProfile aiProfile;
        [SerializeField] protected UnitsScanner unitsDetector;
        [SerializeField] ToggleEvent onServerToggle;

        UnitsCollectionManager s_UnitsCollectionManager;
        State_Ai currState;
        float updateCounter = 0;
        protected Attacker attacker;
        protected NavMeshAgent agent;

        protected virtual void Start()
        {
            if (onServerToggle != null)
            {
                onServerToggle.Invoke(isServer);
            }
            if (isServer)
            {
                s_UnitsCollectionManager = FindObjectOfType<UnitsCollectionManager>();
                int unitID = s_UnitsCollectionManager.GetNewUnitID();
                s_ConnectUnitToEveryone(unitID);
            }
            agent = GetComponent<NavMeshAgent>();
            attacker = GetComponent<Attacker>();
        }
        public void TransitionToState(State_Ai newState)
        {
            if (newState != null && currState != newState)
            {
                if (currState != null)
                {
                    currState.OnExit();
                }
                newState.OnEnter();
                currState = newState;
            }
        }
        protected bool CanUpdate()
        {
            if (isServer == false)
            {
                return false;
            }
            updateCounter += Time.deltaTime / aiProfile.TimeBetweenUpdates;
            if (updateCounter >= 1)
            {
                updateCounter = 0;
                return true;
            }
            return false;
        }
        protected virtual void Update()
        {
            if (CanUpdate() == false)
            {
                return;
            }
            currState.Update();
            TrimmedUpdate();
        }
        protected virtual void TrimmedUpdate() { }
    }
}