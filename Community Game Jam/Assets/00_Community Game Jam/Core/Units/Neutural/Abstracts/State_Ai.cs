﻿namespace CMGJ.AI
{
    public abstract class State_Ai : IState
    {
        NeuturalAI ai;
        public State_Ai(NeuturalAI ai)
        {
            this.ai = ai;
        }
        public abstract void OnEnter();
        public abstract void OnExit();
        public abstract void Update();
    }
}