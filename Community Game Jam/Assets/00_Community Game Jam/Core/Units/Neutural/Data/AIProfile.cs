﻿using UnityEngine;

namespace CMGJ.AI
{
    [CreateAssetMenu(menuName = "CMG/Ai/Ai_Profile", fileName = "Ai_Profile")]
    public class AIProfile : ScriptableObject
    {
        public float TimeBetweenUpdates { get { return timeBetweenUpdates; } }

        [SerializeField] float timeBetweenUpdates = 0.1f;
    }
}