﻿using CMGJ.CameraAndUI;
using UnityEngine.Networking;
using UnityEngine;

namespace CMGJ.Units
{
    public class CoreUnit : Unit
    {
        [Header("Core Unit::Variables")]
        [SerializeField] UnitSpawnInfo[] spawnableUnits;
        [SerializeField] Transform[] spawnPositions;
        [Header("Core Unit::Read Only")]
        [SerializeField] PlayerManager s_MyPlayerManager;

        PlayerQueueMenu playerSpawningMenu;
        UnitsCollectionManager unitsCollectionManager;
        [SerializeField] int currSpawnPos;
        protected override void Start()
        {
            base.Start();
        }
        public override void OnStartAuthority()
        {
            base.OnStartAuthority();
            if (hasAuthority)
            {
                //Subscribe To Unit Queue Finished
                playerSpawningMenu = FindObjectOfType<PlayerQueueMenu>();
                playerSpawningMenu.Sub_OnDoneBuildingUnit(SpawnUnit);
            }
        }
        public override void OnStopAuthority()
        {
            base.OnStopAuthority();
            if (playerSpawningMenu != null)
            {
                playerSpawningMenu.DeSub_OnDoneBuildingUnit(SpawnUnit);
            }
        }

        private void SpawnUnit(UnitSpawnInfo unitSpawnInfo)
        {
            Cmd_SpawnUnit(unitSpawnInfo.UnitID);
        }
        GameObject GetSpawnablePrefab(int unitID)
        {
            foreach (UnitSpawnInfo item in spawnableUnits)
            {
                if (item.UnitID == unitID)
                {
                    return item.UnitPrefab;
                }
            }
            return null;
        }
        private void TrySetPlayerManager()
        {
            if (isServer && s_MyPlayerManager == null)
            {
                //Set My Player
                PlayerManager[] players = FindObjectsOfType<PlayerManager>();
                foreach (PlayerManager pm in players)
                {
                    if (pm.player == owner)
                    {
                        s_MyPlayerManager = pm;
                        break;
                    }
                }
            }
            if (isServer && unitsCollectionManager == null)
            {
                unitsCollectionManager = FindObjectOfType<UnitsCollectionManager>();
            }
        }
        [Command]
        private void Cmd_SpawnUnit(int unitID)
        {
            TrySetPlayerManager();

            GameObject unitPrefab = GetSpawnablePrefab(unitID);
            Vector3 spawnPos = spawnPositions[currSpawnPos].transform.position;
            GameObject spawnedUnitObj = Instantiate(unitPrefab, spawnPos, Quaternion.identity, transform.parent);

            NetworkServer.SpawnWithClientAuthority(spawnedUnitObj, s_MyPlayerManager.connectionToClient);
            int newUnitID = unitsCollectionManager.GetNewUnitID();
            Rpc_SpawnUnitOnAllClients(spawnedUnitObj, newUnitID);
        }

        [ClientRpc]
        private void Rpc_SpawnUnitOnAllClients(GameObject spawnedUnitObj,int newUnitID)
        {
            Vector3 spawnPos = spawnPositions[currSpawnPos].transform.position;
            currSpawnPos = (currSpawnPos + 1) % spawnPositions.Length;
            Unit spawnedUnit = spawnedUnitObj.GetComponent<Unit>();
            if (spawnedUnit == null)
            {
                return;
            }
            spawnedUnit.transform.SetParent(transform.parent);
            if (spawnedUnit.hasAuthority == false)
            {
                return;
            }
            spawnedUnit.SetInitialPosition(spawnPos);
            spawnedUnit.SetUpUnit(owner, newUnitID);
        }
    }
}