﻿using UnityEngine;
using UnityEngine.AI;

namespace CMGJ.Units
{
    public class UnitAnimator : MonoBehaviour
    {
        [SerializeField] BaseUnit unit;
        NavMeshAgent agent;
        Animator animator;
        const string isMoving_KEY = "isMoving";
        const string attack_KEY = "Attack";
        bool isMoving = false;
        Attacker attacker;
        UnitMovement unitMovement;

        private void Start()
        {
            animator = GetComponent<Animator>();
            agent = unit.GetComponent<NavMeshAgent>();
            animator.applyRootMotion = true;
            unitMovement = unit.GetComponent<UnitMovement>();
            attacker = unit.GetComponent<Attacker>();
            unitMovement.OnMovementStarted += OnMovementStarted;
            unitMovement.OnMovementDone += OnMovementDone;

            attacker.OnAttackTriggered += UnitCommander_OnAttackTriggered; ;
            attacker.OnAttackEnded += UnitCommander_OnAttackEnded; ;
            animator.speed = unitMovement.UnitMovementProfile.AnimationSpeedMultiplayer;
        }

        private void UnitCommander_OnAttackTriggered()
        {
            animator.SetTrigger(attack_KEY);
        }
        private void UnitCommander_OnAttackEnded()
        {
            
        }
        private void OnMovementDone()
        {
            isMoving = false;
            animator.SetBool(isMoving_KEY, isMoving);
        }
        private void OnMovementStarted()
        {
            isMoving = true;
            animator.SetBool(isMoving_KEY, isMoving);
        }
        private void OnAnimatorMove()
        {
            if (unitMovement != null && unitMovement.UnitMovementProfile.UseAnimMovement)
            {
                agent.velocity = animator.deltaPosition / Time.deltaTime;
            }
        }
    }
}