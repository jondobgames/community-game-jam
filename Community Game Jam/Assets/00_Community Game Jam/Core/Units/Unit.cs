﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Networking;

namespace CMGJ.Units
{
    [SelectionBase]
    public class Unit : BaseUnit
    {
        public int Owner { get { return owner.ID; } }

        [SerializeField] OwnerSetEvent onOwnerSet;
        [SerializeField] ToggleEvent onEnableByAuthority;

        [Header("Unit::Read Only")]
        [SerializeField] protected Player owner;

        NavMeshAgent agent;
        ISelect[] selects;
        bool didEnableUnit;
        /// <summary>
        /// Server Only Variable
        /// </summary>

        public virtual void SetInitialPosition(Vector3 startPos)
        {
            agent = GetComponent<NavMeshAgent>();
            agent.Warp(startPos);
        }
        public void SetUpUnit(Player owner, int unitID)
        {
            AssignUnitProfileOnallClients(owner, unitID);
        }
        #region SetUpUnitProfile
        private void AssignUnitProfileOnallClients(Player owner, int unitID)
        {
            Cmd_AssignOwnerProfile(owner, unitID);
        }
        [Command]
        private void Cmd_AssignOwnerProfile(Player owner, int unitID)
        {
            this.owner = owner;
            s_ConnectUnitToEveryone(unitID);
            Rpc_AssignProfile(owner, unitID);
        }
        [ClientRpc]
        private void Rpc_AssignProfile(Player owner, int unitID)
        {
            SetUnitID(unitID);
            Local_AssignOwner(owner);
        }
        private void Local_AssignOwner(Player owner)
        {
            this.owner = owner;
            if (onOwnerSet != null)
            {
                onOwnerSet.Invoke(owner);
            }
            if (hasAuthority)
            {
                EnableUnit();
            }
            else
            {
                DisableUnit();
            }
        }
        #endregion
        protected virtual void Start()
        {
            selects = GetComponentsInChildren<ISelect>();
        }
        private void EnableUnit()
        {
            onEnableByAuthority.Invoke(true);
        }
        private void DisableUnit()
        {
            onEnableByAuthority.Invoke(false);
        }
        public bool IsSamewOwner(Player testOwner)
        {
            return owner == testOwner;
        }
        public void Select()
        {
            foreach (ISelect s in selects)
            {
                s.Select();
            }
        }
        public void DeSelect()
        {
            foreach (ISelect s in selects)
            {
                s.DeSelect();
            }
        }
    }
}