﻿using UnityEngine;
using System;
using UnityEngine.AI;
using UnityEngine.Networking;
namespace CMGJ.Units
{
    public class UnitMovement : NetworkBehaviour
    {
        public UnitMovementProfile UnitMovementProfile { get { return unitMovementProfile; } }

        public event Action OnMovementStarted;
        public event Action OnMovementDone;

        [SerializeField] UnitMovementProfile unitMovementProfile;
        [Tooltip("if left empty, it will use the transform that holds the UnitMovement Component")]
        [SerializeField] Transform rotatableTransform;
        /// <summary>
        /// when movementStarts, it sends true, when it ends it sends false
        /// </summary>
        [Tooltip("when movementStarts, it sends true, when it ends it sends false")]
        [SerializeField] ToggleEvent onMovementStarted;
        [SerializeField] ToggleEvent onMovementDone;
        
        private NavMeshAgent agent;

        bool isMoving = false;
        Quaternion targetRot;
        bool specialLooking = false;
        private void Reset()
        {
            rotatableTransform = transform;
        }
        private void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            agent.updateRotation = false;
            agent.speed = unitMovementProfile.MovementSpeed;
            agent.stoppingDistance = unitMovementProfile.StoppingDistance;
            if (rotatableTransform==null)
            {
                rotatableTransform = transform;
            }
            isMoving = false;
        }

        private void Update()
        {
            if (hasAuthority==false)
            {
                return;
            }
            CheckMovementDone();
            UpdateRotation();
        }

        private void CheckMovementDone()
        {
            if (agent.pathPending == false && agent.remainingDistance <= agent.stoppingDistance && isMoving == true)
            {
                isMoving = false;
                specialLooking = false;
                ServerMovementDone();
            }
        }
        private void UpdateRotation()
        {
            if (specialLooking == false)
            {
                Vector3 dir = agent.desiredVelocity;
                if (dir == Vector3.zero)
                {
                    return;
                }
                targetRot = Quaternion.LookRotation(dir);
            }
            rotatableTransform.rotation = Quaternion.RotateTowards(rotatableTransform.rotation, targetRot, unitMovementProfile.TurnSpeed * Time.deltaTime);
        }

        public void UpdateTurningTo(Quaternion targetRot)
        {
            this.targetRot = targetRot;
            specialLooking = true;
        }
        public void Move(Vector3 pos, Quaternion lookTarget)
        {
            if (hasAuthority==false)
            {
                return;
            }
            Move(pos);
            targetRot = lookTarget;
            specialLooking = true;
        }
        public void Move(Vector3 position)
        {
            if (hasAuthority==false)
            {
                return;
            }
            agent.destination = position;
            if (isMoving == false)
            {
                isMoving = true;
                ServerStartMovement();
            }
        }
        public void ClearFocus()
        {
            specialLooking = false;
        }
        #region NetworkCalls
        private void ServerStartMovement()
        {
            Cmd_StartMovement();
        }
        private void ServerMovementDone()
        {
            Cmd_MovementDone();
        }
        [Command]
        private void Cmd_StartMovement()
        {
            Rpc_StartMovement();
        }
        [ClientRpc]
        private void Rpc_StartMovement()
        {
            if (onMovementStarted != null)
            {
                onMovementStarted.Invoke(true);
            }
            if (OnMovementStarted != null)
            {
                OnMovementStarted();
            }
        }
        [Command]
        private void Cmd_MovementDone()
        {
            Rpc_MovementDone();
        }
        [ClientRpc]
        private void Rpc_MovementDone()
        {
            if (onMovementDone!=null)
            {
                onMovementDone.Invoke(true);
            }
            if (OnMovementDone != null)
            {
                OnMovementDone();
            }
        }
        #endregion
    }
}