﻿using UnityEngine;
using UnityEngine.Networking;
namespace CMGJ.Units
{
    public class UnitCommander : NetworkBehaviour
    {
        UnitMovement unitMovement;
        Attacker attacker;
        public override void OnStartAuthority()
        {
            base.OnStartAuthority();
            if (hasAuthority==false)
            {
                return;
            }
            unitMovement = GetComponent<UnitMovement>();
            attacker = GetComponent<Attacker>();
        }
        public void MoveTo(Vector3 position)
        {
            if (hasAuthority==false)
            {
                return;
            }
            attacker.ClearFocus();
            unitMovement.ClearFocus();
            unitMovement.Move(position);
        }
        public void Attack(BaseUnit unit,Vector3 formationVector)
        {
            if (hasAuthority == false|| unit==null)
            {
                return;
            }
            Health unitHealth = unit.GetComponentInChildren<Health>();
            if (unitHealth)
            {
                attacker.Charge(unit, formationVector);
            }
        }
    }
}