﻿using UnityEngine;
using UnityEngine.Networking;

namespace CMGJ
{
    public class UnitRotationSyncer : UnitSyncer
    {
        [Header("Rotation-Sync Specific")]
        [SerializeField] float rotationLerpTime = 40.0f;
        [SerializeField] float rotationSyncThreshold = 3.0f;

        [Header("Read Only")]
        [SerializeField] float serverRotation;
        
        protected override bool CanSendDataToServer(bool forceUpdate)
        {
            float angleDifference = Mathf.Abs(serverRotation - GetRotation());
            return angleDifference > rotationSyncThreshold || forceUpdate;
        }
        protected override void AuthorityUpdate()
        {
            float tempNewServerRotation = GetRotation();
            if (serverRotation == tempNewServerRotation || tempNewServerRotation == 0)
            {
                return;
            }
            //Debug.Log("sending " + tempNewServerRotation + " to the server");
            Cmd_SendDataToServer(tempNewServerRotation);
        }

        [Command]
        private void Cmd_SendDataToServer(float newServerRotation)
        {
            serverRotation = newServerRotation;
            //Debug.Log("got " + serverRotation + " from authoritive copy");
            Rpc_UpdateServerRotOnClients(newServerRotation);
        }
        [ClientRpc]
        private void Rpc_UpdateServerRotOnClients(float newServerRotation)
        {
            serverRotation = newServerRotation;
            //Debug.Log("got " + serverRotation+ " from server command");
        }
        protected override void GetCloserToServerData()
        {
            Quaternion targetRot = Quaternion.Euler(0, serverRotation, 0);
            if (syncLocal)
            {
                toSync.localRotation = Quaternion.Lerp(toSync.localRotation, targetRot, rotationLerpTime * Time.deltaTime);
                return;
            }
            toSync.rotation = Quaternion.Lerp(toSync.rotation, targetRot, rotationLerpTime * Time.deltaTime);
        }
        private Quaternion GetQRot()
        {
            Quaternion q = new Quaternion();
            if (syncLocal)
            {
                q = toSync.localRotation;
            }
            else
            {
                q = toSync.rotation;
            }
            return q;
        }
        private float GetRotation()
        {
            float newServerRotationToSet;
            if (syncLocal)
            {
                newServerRotationToSet = toSync.localRotation.eulerAngles.y;
            }
            else
            {
                newServerRotationToSet = toSync.rotation.eulerAngles.y;
            }
            return newServerRotationToSet;
        }

        protected override void ForcedUnrestrictedSync()
        {
            float tempNewServerRotation = GetRotation();
            Cmd_SendDataToServer(tempNewServerRotation);
        }
    }
}