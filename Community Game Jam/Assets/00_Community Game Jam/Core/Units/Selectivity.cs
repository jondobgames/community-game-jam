﻿using UnityEngine;

namespace CMGJ.Units
{
    public class Selectivity : MonoBehaviour
    {
        public BaseUnit Unit { get { return unit; } }
        [SerializeField] BaseUnit unit;
        private void Reset()
        {
            unit = transform.parent.GetComponent<BaseUnit>();
        }
        private void Start() { }
    }
}