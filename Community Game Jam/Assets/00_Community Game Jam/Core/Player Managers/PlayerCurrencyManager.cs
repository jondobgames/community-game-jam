﻿using System;
using CMGJ.Units;
using UnityEngine;

namespace CMGJ
{
    public class PlayerCurrencyManager : MonoBehaviour
    {
        public event Action<Currency> OnCurrencyUpdated;
        [SerializeField] Currency playerCurrency;
        bool gameStarted = false;
        public bool CanAfford(UnitSpawnInfo unitSpawnInfo)
        {
            return playerCurrency.CanAfford(unitSpawnInfo.Cost);
        }
        public void Addfunds(float byAmount)
        {
            playerCurrency.Add(byAmount);
            UpdateCurrency(playerCurrency);
        }
        public void Buy(UnitSpawnInfo unitSpawnInfo)
        {
            playerCurrency.Subtract(unitSpawnInfo.Cost);
            UpdateCurrency(playerCurrency);
        }
        public void StartMining()
        {
            gameStarted = true;

            if (OnCurrencyUpdated!=null)
            {
                OnCurrencyUpdated(playerCurrency);
            }
        }
        
        private void Update()
        {
            if (gameStarted && playerCurrency.Update())
            {
                UpdateCurrency(playerCurrency);
            }
        }
        public void UpdateCurrency(Currency playerCurrency)
        {
            this.playerCurrency = playerCurrency;

            if (OnCurrencyUpdated != null)
            {
                OnCurrencyUpdated(playerCurrency);
            }
        }
    }
}