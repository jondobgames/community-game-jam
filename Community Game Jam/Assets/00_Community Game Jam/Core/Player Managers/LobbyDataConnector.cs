﻿using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.Networking;

namespace CMGJ
{
    public class LobbyDataConnector : LobbyHook
    {
        public override void OnLobbyServerSceneLoadedForPlayer
            (NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
        {
            PlayerManager pm = gamePlayer.GetComponent<PlayerManager>();
            LobbyPlayer lp = lobbyPlayer.GetComponent<LobbyPlayer>();
            pm.s_RecieveLobbyData(lp.playerColor, lp.playerName);
        }
    }
}