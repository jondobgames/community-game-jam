﻿using UnityEngine;

namespace CMGJ
{
    public class MouseManager
    {
        const int right = 1;
        const int left = 0;

        bool IsClickUp(int mouseButton)
        {
            return Input.GetMouseButtonUp(mouseButton);
        }
        public bool IsLeftUp()
        {
            return IsClickUp(left);
        }
        public bool IsRightUp()
        {
            return IsClickUp(right);
        }
        bool IsClickHeld(int mouseButton)
        {
            return Input.GetMouseButton(mouseButton);
        }
        public bool IsLeftHeld()
        {
            return IsClickHeld(left);
        }
        public bool IsRightHeld()
        {
            return IsClickHeld(right);
        }
        bool IsMouseDown(int mouseButton)
        {
            return Input.GetMouseButtonDown(mouseButton);
        }
        public bool IsLeftDown()
        {
            return IsMouseDown(left);
        }
        public bool IsRightDown()
        {
            return IsMouseDown(right);
        }
    }
}