﻿using CMGJ.CameraAndUI;
using CMGJ.Units;
using System;
using UnityEngine;

namespace CMGJ
{
    public class PlayerManagersHelper : MonoBehaviour
    {
        public event Action<Player> onOwnerSet;
        [Header("Read Only")]
        [SerializeField] Player owner;
        [SerializeField] RectTransform tempUIElementsParent;
        [SerializeField] Transform tempObjsParent;
        Transform headTransform;
        CameraController cameraController;

        /// <summary>
        /// parent of all current player objects live
        /// </summary>
        public Transform HeadTransform { get { return headTransform; } }
        /// <summary>
        /// camera controller of player
        /// </summary>
        public CameraController CameraController { get { return cameraController; } }
        /// <summary>
        /// ray caster of the player
        /// </summary>
        public CameraRayCaster RayCaster
        {
            get
            {
                if (cameraController==null)
                {
                    cameraController = FindObjectOfType<CameraController>();
                }
                return cameraController.RayCaster();
            }
        }
        /// <summary>
        /// the owner of this managers helper
        /// </summary>
        public Player Owner { get { return owner; } }

        public Transform TempObjsParent { get { return tempObjsParent; } }
        public Transform TempUIElementsParent { get { return tempUIElementsParent; } }
        CoreUnit activeCoreUnit;
        public CoreUnit GetActiveCoreUnit()
        {
            if (activeCoreUnit)
            {
                return activeCoreUnit;
            }
            CoreUnit[] allCores = FindObjectsOfType<CoreUnit>();
            for (int i = 0; i < allCores.Length; i++)
            {
                if (allCores[i].IsSamewOwner(owner))
                {
                    activeCoreUnit = allCores[i];
                    break;
                }
            }
            return activeCoreUnit;
        }
        public bool IsOwner(Player testOwner)
        {
            return testOwner == owner;
        }
        public void SetUp(Player owner, Transform headTransform) 
        {
            this.owner = owner;
            this.headTransform = headTransform;
            name = "Managers_" + owner.FullPlayerName;
            if (onOwnerSet!=null)
            {
                onOwnerSet(this.owner);
            }
        }
    }
}