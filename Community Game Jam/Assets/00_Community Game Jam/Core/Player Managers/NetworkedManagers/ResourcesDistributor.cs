﻿using UnityEngine.Networking;
namespace CMGJ
{
    public class ResourcesDistributor : NetworkBehaviour
    {
        NetworkedPlayerManagersTracker tracker;
        private void Start()
        {
            tracker = GetComponent<NetworkedPlayerManagersTracker>();
        }
        public void s_GiveMoneyTo(int playerID, float amount)
        {
            PlayerManager pm = GetPlayerManager(playerID);
            pm.Cmd_TakeMoney(amount, playerID);
        }
        PlayerManager GetPlayerManager(int ofID)
        {
            foreach (PlayerManager pm in tracker.PlayersManagers)
            {
                if (ofID==pm.player.ID)
                {
                    return pm;
                }
            }
            return null;
        }
    }
}