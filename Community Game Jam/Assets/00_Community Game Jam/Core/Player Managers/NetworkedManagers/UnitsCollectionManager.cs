﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace CMGJ.Units
{
    public class UnitsCollectionManager : NetworkBehaviour
    {
        [SerializeField] List<BaseUnit> createdUnits = new List<BaseUnit>();
        [SerializeField] List<int> unitsIDS = new List<int>();

        NetworkedPlayerManagersTracker tracker;
        private void Start()
        {
            tracker = GetComponent<NetworkedPlayerManagersTracker>();
        }
        public int GetNewUnitID()
        {
            return unitsIDS.Count;
        }
        public void AddUnit(BaseUnit unit,int unitID)
        {
            createdUnits.Add(unit);
            unitsIDS.Add(unitID);
        }
        public BaseUnit GetUnitOf(int id)
        {
            int index = GetIndexOf(id);
            if (index!=-1)
            {
                return createdUnits[index];
            }
            return null;
        }
        private int GetIndexOf(int id)
        {
            for (int i = 0; i < unitsIDS.Count; i++)
            {
                if (unitsIDS[i] == id)
                {
                    return i;
                }
            }
            return -1;
        }

        public void RelayCurrentUnitIDS()
        {
            for (int i = 0; i < createdUnits.Count; i++)
            {
                createdUnits[i].ConnectID();
            }
        }

        public bool s_HasUnits(int player)
        {
            foreach (BaseUnit bu in createdUnits) 
            {
                Unit u = bu as Unit;
                if (bu == null || u==null || u.Owner != player)
                {
                    continue;
                }
                return true;
            }
            return false;
        }
        public bool s_IsGameOver()
        {
            foreach (PlayerManager pm in tracker.PlayersManagers)
            {
                if (s_HasUnits(pm.player.ID) == false)
                {
                    return true;
                }
            }
            return false;
        }
    }
}