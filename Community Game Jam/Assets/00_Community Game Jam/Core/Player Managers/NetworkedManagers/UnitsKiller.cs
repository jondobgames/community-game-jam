﻿using UnityEngine;
using UnityEngine.Networking;

namespace CMGJ
{
    public class UnitsKiller : NetworkBehaviour
    {
        NetworkedPlayerManagersTracker tracker;

        private void Start()
        {
            tracker = GetComponent<NetworkedPlayerManagersTracker>();
        }
        public void s_KillUnit(GameObject objToKill)
        {
            NetworkServer.Destroy(objToKill);
            Rpc_KillForEveryone(objToKill.GetComponent<NetworkIdentity>().netId);
            foreach (PlayerManager pm in tracker.PlayersManagers)
            {
                pm.s_CheckWinConds();
            }
        }
        [ClientRpc]
        private void Rpc_KillForEveryone(NetworkInstanceId unitID)
        {
            GameObject g = ClientScene.FindLocalObject(unitID);
            Destroy(g);
        }

    }
}