﻿using CMGJ.CameraAndUI;
using CMGJ.Units;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

namespace CMGJ
{
    public class PlayerManager : NetworkBehaviour
    {
        [Header("Core")] public Player player;

        [Header("Player Parts")]
        [SerializeField] CoreUnit coreUnitObj;

        CoreUnit coreUnit;
        //server variables
        UnitsCollectionManager s_UnitsCollectionManager;
        NetworkedPlayerManagersTracker s_NetPlayerManagersTracker;

        Color s_PlayerColor = Color.green;
        string s_PlayerName = "Player 00";
        public void s_RecieveLobbyData(Color playerColor, string playerName)
        {
            s_PlayerColor = playerColor;
            s_PlayerName = playerName;
        }

        [Command]
        private void Cmd_ApplyLobbyData()
        {
            player.accentColor = s_PlayerColor;
            player.playerName = s_PlayerName;
            Rpc_RecieveLobbyData(s_PlayerColor, s_PlayerName);
        }
        [ClientRpc]
        private void Rpc_RecieveLobbyData(Color playerColor, string playerName)
        {
            player.accentColor = playerColor;
            player.playerName = playerName;
        }

        //local playerVariables
        PlayerCurrencyManager l_PlayerCurrencyManager;

        public NetworkedPlayerManagersTracker S_NetPlayerManagersTracker
        {
            get
            {
                if (s_NetPlayerManagersTracker == null)
                {
                    s_NetPlayerManagersTracker = FindObjectOfType<NetworkedPlayerManagersTracker>();
                }
                return s_NetPlayerManagersTracker;
            }
        }
        public PlayerCurrencyManager L_PlayerCurrencyManager
        {
            get
            {
                if (l_PlayerCurrencyManager == null)
                {
                    l_PlayerCurrencyManager = FindObjectOfType<PlayerCurrencyManager>();
                }
                return l_PlayerCurrencyManager;
            }
        }
        public UnitsCollectionManager S_UnitsCollectionManager
        {
            get
            {
                if (s_UnitsCollectionManager == null)
                {
                    s_UnitsCollectionManager = FindObjectOfType<UnitsCollectionManager>();
                }
                return s_UnitsCollectionManager;
            }
        }

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(2f);
            if (isLocalPlayer)
            {
                l_PlayerCurrencyManager = FindObjectOfType<PlayerCurrencyManager>();
            }
            if (isServer)
            {
                s_UnitsCollectionManager = FindObjectOfType<UnitsCollectionManager>();
                s_NetPlayerManagersTracker = FindObjectOfType<NetworkedPlayerManagersTracker>();
                InitPlayer();
            }
        }


        public void s_CheckWinConds() 
        {
            StartCoroutine(CheckWinConds());
        }

        private IEnumerator CheckWinConds()
        {
            yield return new WaitForSeconds(1);
            bool gameOver = S_UnitsCollectionManager.s_IsGameOver();
            bool hasUnits = S_UnitsCollectionManager.s_HasUnits(player.ID);
            if (gameOver)
            {
                Rpc_PlayerManager(hasUnits);
            }
        }

        [ClientRpc]
        private void Rpc_PlayerManager(bool hasUnits)
        {
            if (isLocalPlayer == false)
            {
                return;
            }
            FindObjectOfType<GameStateManager>().SetGameOverState(hasUnits);
        }

        [Command]
        public void Cmd_TakeMoney(float amount, int playerID)
        {
            Rpc_AddFundsToPlayer(amount, playerID);
        }
        [ClientRpc]
        private void Rpc_AddFundsToPlayer(float amount, int playerID)
        {
            if (isLocalPlayer && player.ID == playerID)
            {
                L_PlayerCurrencyManager.Addfunds(amount);
            }
        }

        private void InitPlayer()
        {
            Cmd_ApplyLobbyData();
            Cmd_UpdatePlayer();
            Cmd_SpawnCoreUnit();
            Cmd_TriggerResources();
        }

        [Command]
        private void Cmd_TriggerResources()
        {
            Rpc_TriggerResources();
        }
        [ClientRpc]
        private void Rpc_TriggerResources()
        {
            if (isLocalPlayer)
            {
                L_PlayerCurrencyManager.StartMining();
            }
        }

        [Command]
        private void Cmd_UpdatePlayer()
        {
            player = player.GetPreparedPlayer();
            name = player.FullPlayerName;
            S_NetPlayerManagersTracker.AddPlayer(this);
            Rpc_UpdatePlayer(player);
        }
        [ClientRpc]
        private void Rpc_UpdatePlayer(Player p)
        {
            player = p;
            name = player.FullPlayerName;
            if (isLocalPlayer)
            {
                FindObjectOfType<PlayerManagersHelper>().SetUp(player, transform);
            }
        }
        [Command]
        private void Cmd_SpawnCoreUnit()
        {
            coreUnit = Instantiate(coreUnitObj, transform.position, Quaternion.identity, transform);
            coreUnit.SetInitialPosition(transform.position);

            NetworkServer.SpawnWithClientAuthority(coreUnit.gameObject, connectionToClient);
            int newUnitID = S_UnitsCollectionManager.GetNewUnitID();
            S_UnitsCollectionManager.RelayCurrentUnitIDS();

            Rpc_SetUpUnit(coreUnit.gameObject, newUnitID);
        }
        [ClientRpc]
        private void Rpc_SetUpUnit(GameObject unitObj, int newUnitID)
        {
            CoreUnit u = unitObj.GetComponent<CoreUnit>();
            if (u == null)
            {
                return;
            }
            u.transform.SetParent(transform);

            if (u.hasAuthority == false)
            {
                return;
            }

            coreUnit = u;
            u.SetUpUnit(player, newUnitID);
            CameraController cc = FindObjectOfType<CameraController>();
            if (cc)
            {
                cc.SetUp(u.transform);
            }
        }
    }
}