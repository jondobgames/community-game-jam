﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace CMGJ
{
    public class NetworkedPlayerManagersTracker : NetworkBehaviour
    {
        public List<PlayerManager> PlayersManagers { get { return playersManagers; } }

        [Header("Read Only")]
        [SerializeField] List<PlayerManager> playersManagers = new List<PlayerManager>();

        public void AddPlayer(PlayerManager playerManager)
        {
            playersManagers.Add(playerManager);
        }
    }
}