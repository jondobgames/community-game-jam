﻿using CMGJ.Units;
using System.Collections.Generic;
using UnityEngine;

namespace CMGJ
{
    public class FormationsManager : MonoBehaviour
    {
        enum FormationType { NaturalFormation }
        [SerializeField] FormationType formationType = FormationType.NaturalFormation;

        NaturalFormation naturalFormation = new NaturalFormation();
        public List<Vector3> GetCoreVectors<T>(List<T> units, Vector3 to = default(Vector3)) where T : Component
        {
            switch (formationType)
            {
                case FormationType.NaturalFormation:
                    return naturalFormation.GetCoreVectors(units);
            }
            return new List<Vector3>();
        }
        public List<Vector3> GetMovementTargets<T>(List<T> units, Vector3 to) where T : Component
        {
            switch (formationType)
            {
                case FormationType.NaturalFormation:
                    return naturalFormation.GetFormattedPoints(units, to);
            }
            return new List<Vector3>();
        }
    }
}