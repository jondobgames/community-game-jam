﻿using System.Collections.Generic;
using UnityEngine;

namespace CMGJ.Units
{
    public class NaturalFormation : IFormation
    {
        public List<Vector3> GetFormattedPoints<T>(List<T> units, Vector3 to) where T : Component
        {
            List<Vector3> dampVectors = GetDampVectors(units);
            List<Vector3> targetPositions = new List<Vector3>();
            for (int i = 0; i < units.Count; i++)
            {
                Vector3 targetPosition = dampVectors[i] + to;
                targetPositions.Add(targetPosition);
            }
            return targetPositions;
        }

        public List<Vector3> GetCoreVectors<T>(List<T> units, Vector3 to = default(Vector3)) where T : Component
        {
            return GetDampVectors(units);
        }

        /// <summary>
        /// vectors, that offset the units, from their average position
        /// </summary>
        /// <param name="units"></param>
        /// <returns></returns>
        List<Vector3> GetDampVectors<T>(List<T> units) where T:Component
        {
            List<Vector3> dampVectors = new List<Vector3>();
            List<Vector3> startPositions = units.GetPositions();
            Vector3 averagePos = units.GetAveragePosition();

            foreach (Vector3 startPos in startPositions)
            {
                //minus where we end;
                Vector3 dampVector = -averagePos + startPos;
                dampVectors.Add(dampVector);
            }
            return dampVectors;
        }
    }
}