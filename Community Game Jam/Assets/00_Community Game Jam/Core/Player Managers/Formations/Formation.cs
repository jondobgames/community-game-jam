﻿using System.Collections.Generic;
using UnityEngine;

namespace CMGJ.Units
{
    public interface IFormation
    {
        List<Vector3> GetFormattedPoints<T>(List<T> units, Vector3 to) where T : Component;
        List<Vector3> GetCoreVectors<T>(List<T> units, Vector3 to) where T : Component;
    }
}