﻿using System;
using UnityEngine;
using R = UnityEngine.Random;

namespace CMGJ
{
    [Serializable]
    public class Player
    {
        public string FullPlayerName { get { return playerName + "_" + id; } }
        public Color AccentColor { get { return accentColor; } }
        public int ID { get { return id; } }

        public string playerName;
        public Color accentColor;
        public int id;

        public Player(string playerName, Color accentColor, int id)
        {
            this.playerName = playerName;
            this.accentColor = accentColor;
            this.id = id;
        }
        public Player()
        {
            playerName = "XXXXXXXX";
            accentColor = Color.white;
            id = 0;
        }
        public Player GetPreparedPlayer()
        {
            int id = R.Range(int.MinValue, int.MaxValue);
            return new Player(playerName, accentColor, id);
        }
        public static Player NewRandomPlayer()
        {
            Color c = new Color(R.value, R.value, R.value);
            int id = R.Range(0, int.MaxValue);
            return new Player("Player" + "_" + id, c,id);
        }

        public static bool operator ==(Player p1, Player p2)
        {
            return p1.ID == p2.ID;
        }
        public static bool operator !=(Player p1, Player p2)
        {
            return !(p1 == p2);
        }
        public override bool Equals(object obj)
        {
            return id == ((Player)obj).id;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}