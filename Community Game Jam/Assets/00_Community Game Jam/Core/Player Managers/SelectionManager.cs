﻿using CMGJ.CameraAndUI;
using System;
using System.Collections.Generic;
using UnityEngine;
namespace CMGJ.Units
{
    public class SelectionManager : MonoBehaviour
    {
        public bool HasSelections { get { return selectedUnits.Count > 0; } }
        public MouseManager mouseManager { get { return mm; } }

        
        [SerializeField] SelectionUI selectionUI;
        [SerializeField] float timeTillHeld = 0.05f;
        [Header("Read Only")]
        [SerializeField] List<Unit> selectedUnits = new List<Unit>();
        [SerializeField] float holdingCounter = 0;

        MouseManager mm;
        CameraRayCaster rayCaster;
        PlayerManagersHelper playerManagersHelper;
        bool IsHoldingLeft { get { return holdingCounter > 1; } }
        FormationsManager formationsManager;

        private void Start()
        {
            playerManagersHelper = GetComponent<PlayerManagersHelper>();
            mm = new MouseManager();
            formationsManager = GetComponent<FormationsManager>();
            rayCaster = playerManagersHelper.RayCaster;
            rayCaster.OnMouseOverTerrainEvent += Instance_OnMouseOverTerrain;
            rayCaster.OnMouseOverUnitEvent += RayCaster_OnMouseOverUnitEvent;
        }
        private void Update()
        {
            if (mm.IsLeftDown())
            {
                selectionUI.SetStartPosition();
                return;
            }
            if (mm.IsLeftUp())
            {
                if (IsHoldingLeft)
                {
                    ClearSelection();
                    Ray startRay, endRay;
                    selectionUI.ReSetSelectionBox(out startRay, out endRay);
                    List<BaseUnit> unitsInSelectionBox = rayCaster.BoxCastForUnits(startRay, endRay);
                    ProcessUnits(unitsInSelectionBox);
                    return;
                }
                selectionUI.ReSetSelectionBox();
                return;
            }
            if (mm.IsLeftHeld())
            {
                holdingCounter += Time.deltaTime / timeTillHeld;
                if (IsHoldingLeft)
                {
                    selectionUI.UpdateSelectionBox();
                }
                return;
            }
            if (holdingCounter != 0)
            {
                holdingCounter = 0;
            }
        }
      
        private void RayCaster_OnMouseOverUnitEvent(BaseUnit unit)
        {
            if (mm.IsRightDown())
            {
                Unit u = unit as Unit;
                selectedUnits.RemoveAll(IsUnitNull);
                List<Vector3> coreVectors = formationsManager.GetCoreVectors(selectedUnits, unit.transform.position);
                for (int i = 0; i < selectedUnits.Count; i++)
                {
                    if (u)
                    {
                        if (IsOwnedByMe(u))
                        {
                            continue;
                        }
                        selectedUnits[i].GetComponent<UnitCommander>().Attack(u, coreVectors[i]);
                        continue;
                    }
                    selectedUnits[i].GetComponent<UnitCommander>().Attack(unit, coreVectors[i]);
                    
                }
            }
            if (mm.IsLeftDown() == false)
            {
                return;
            }
            ClearSelection();
            ProcessUnit(unit);
        }

        private void Instance_OnMouseOverTerrain(Vector3 position)
        {
            if (mm.IsLeftDown())
            {
                ClearSelection();
                return;
            }
            if (mm.IsRightDown())
            {
                selectedUnits.RemoveAll(IsUnitNull);
                List<Vector3> movementTargets = formationsManager.GetMovementTargets(selectedUnits, position);
                for (int i = 0; i < movementTargets.Count; i++)
                {
                    selectedUnits[i].GetComponent<UnitCommander>().MoveTo(movementTargets[i]);
                }
            }
        }

        private bool IsUnitNull(Unit obj)
        {
            return obj == null;
        }

        void ProcessUnits(List<BaseUnit> unitsInSelectionBox)
        {
            foreach (BaseUnit unit in unitsInSelectionBox)
            {
                ProcessUnit(unit);
            }
        }
        void ProcessUnit(BaseUnit unitToProcess)
        {
            Unit u = unitToProcess as Unit;
            if (u && IsOwnedByMe(u))
            {
                if (selectedUnits.Contains(u) == false)
                {
                    u.Select();
                    selectedUnits.Add(u);
                }
            }
        }
        bool IsOwnedByMe(Unit u)
        {
            if (u==null)
            {
                return false;
            }
            return u.IsSamewOwner(playerManagersHelper.Owner);
        }
        private void ClearSelection()
        {
            foreach (Unit u in selectedUnits)
            {
                u.DeSelect();
            }
            selectedUnits.Clear();
        }
    }
}