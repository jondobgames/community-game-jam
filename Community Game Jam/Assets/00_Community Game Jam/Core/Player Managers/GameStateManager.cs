﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using CMGJ.CameraAndUI;

namespace CMGJ
{
    public class GameStateManager : MonoBehaviour
    {
        [SerializeField] int lobbySceneBuildIndex = 0;
        [SerializeField] string winText = "Congratulations You Are A Victorious";
        [SerializeField] string loseText = "You Have Been Defeated";
        [SerializeField] float timeToGoToLobby = 3.0f;
        [SerializeField] TextMeshProUGUI gameEndText;
        [SerializeField] GameObject gameEndUIHead;

        public void SetGameOverState(bool didWin)
        {
            gameEndUIHead.SetActive(true);
            string result;
            if (didWin)
            {
                result = winText;
            }
            else
            {
                result = loseText;
            }
            gameEndText.text = result;
            FindObjectOfType<PlayerUIManager>().Exit();
            StartCoroutine(GoToLobby());
        }

        private IEnumerator GoToLobby()
        {
            yield return new WaitForSeconds(timeToGoToLobby);
            SceneManager.LoadScene(lobbySceneBuildIndex);
        }
    }
}