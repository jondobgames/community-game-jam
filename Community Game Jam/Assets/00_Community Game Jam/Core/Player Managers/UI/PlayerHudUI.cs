﻿using CMGJ.Units;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
namespace CMGJ.CameraAndUI
{
    public class PlayerHudUI : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI titleText;
        [SerializeField] TextMeshProUGUI descriptionText;
        [SerializeField] TextMeshProUGUI healthText;
        [SerializeField] Image unitImage;
        [SerializeField] Slider healthSlider;

        public void Display(UnitInfo unitInfo, float currHealth, float totalHealth)
        {
            titleText.text = unitInfo.UnitName;
            descriptionText.text = unitInfo.UnitDescription;
            if (unitInfo.UnitIcon != null)
            {
                unitImage.sprite = unitInfo.UnitIcon;
            }
            healthText.text = currHealth + "/" + totalHealth;
            healthSlider.value = currHealth / totalHealth;
        }
    }
}