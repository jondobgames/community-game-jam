﻿using UnityEngine;
using TMPro;

namespace CMGJ.CameraAndUI
{
    public class PlayerCurrencyUI : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI currencyText;
        private void Awake()
        {
            FindObjectOfType<PlayerCurrencyManager>().OnCurrencyUpdated += PlayerCurrencyUI_OnCurrencyUpdated;
        }
        private void PlayerCurrencyUI_OnCurrencyUpdated(Currency obj)
        {
            DisplayCurrentCurrency(obj.currentCurrency);
        }
        private void DisplayCurrentCurrency(float currency)
        {
            currencyText.text = currency + "$";
        }
    }
}