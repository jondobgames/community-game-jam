﻿using UnityEngine;

namespace CMGJ.CameraAndUI
{
    public abstract class MenuState : IState
    {
        protected PlayerMenu playerMenu;
        public MenuState(PlayerMenu playerMenu)
        {
            this.playerMenu = playerMenu;
        }
        public abstract void OnEnter();
        public abstract void OnExit();
        public abstract void Update();
    }
    public class Menu_Entry : MenuState
    {
        private Animator animator;
        const string Entry_KEY = "Entry";
        const string stateName_KEY = "Entry";
        AnimatorStateInfo stateInfo;
        public Menu_Entry(PlayerMenu playerSpawningMenu, Animator animator) : base(playerSpawningMenu)
        {
            this.animator = animator;
        }
        public override void OnEnter()
        {
            animator.SetTrigger(Entry_KEY);
        }
        public override void OnExit()
        {
        }
        public override void Update()
        {
            stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            if (stateInfo.IsName(stateName_KEY))
            {
                return;
            }
            playerMenu.TransitionToState(playerMenu.UpdatingState());
        }
    }
    public class Menu_Outro : MenuState
    {
        private Animator animator;
        const string Outro_KEY = "Outro";
        const string stateName_KEY = "Outro";
        AnimatorStateInfo stateInfo;
        public Menu_Outro(PlayerMenu playerSpawningMenu, Animator animator) : base(playerSpawningMenu)
        {
            this.animator = animator;
        }
        public override void OnEnter()
        {
            animator.SetTrigger(Outro_KEY);
        }
        public override void OnExit()
        {
        }
        public override void Update()
        {
            stateInfo = animator.GetCurrentAnimatorStateInfo(0);
            if (stateInfo.IsName(stateName_KEY))
            {
                return;
            }
            playerMenu.TransitionToState(playerMenu.idling);
        }
    }
    public class Menu_Updating : MenuState
    {
        public Menu_Updating(PlayerMenu playerSpawningMenu) : base(playerSpawningMenu)
        {
        }
        public override void OnEnter()
        {
        }
        public override void OnExit()
        {
        }
        public override void Update()
        {
        }
    }
    public class Menu_Idling : MenuState
    {
        public Menu_Idling(PlayerMenu playerSpawningMenu) : base(playerSpawningMenu)
        {
        }
        public override void OnEnter()
        {
        }
        public override void OnExit()
        {
        }
        public override void Update()
        {
        }
    }
}