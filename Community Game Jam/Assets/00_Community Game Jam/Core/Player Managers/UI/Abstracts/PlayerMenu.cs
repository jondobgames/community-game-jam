﻿using UnityEngine;

namespace CMGJ.CameraAndUI
{
    public abstract class PlayerMenu : MonoBehaviour
    {
        public Menu_Entry entry;
        public Menu_Outro outro;
        public Menu_Idling idling;
        Menu_Updating updating;

        [SerializeField] Animator menuAnimator;
        protected IState currState;
        protected virtual void Start()
        {
            entry = new Menu_Entry(this, menuAnimator);
            outro = new Menu_Outro(this, menuAnimator);
            idling = new Menu_Idling(this);
            updating = new Menu_Updating(this);

            TransitionToState(idling);
        }
        public virtual Menu_Updating UpdatingState()
        {
            return updating;
        }
        public void TransitionToState(IState newState)
        {
            if (currState != newState)
            {
                if (currState != null)
                {
                    currState.OnExit();
                }
                currState = newState;
                currState.OnEnter();
            }
        }

        protected virtual void Update()
        {
            if (currState != null)
            {
                currState.Update();
            }
        }
    }
}