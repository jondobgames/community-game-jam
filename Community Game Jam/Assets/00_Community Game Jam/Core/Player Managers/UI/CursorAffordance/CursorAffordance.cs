﻿using UnityEngine;
using UnityEngine.EventSystems;
using CMGJ.Units;

namespace CMGJ.CameraAndUI
{
    [RequireComponent(typeof(CameraRayCaster))]
    public class CursorAffordance : MonoBehaviour
    {
        [SerializeField] GameObject uiClickSpot;
        [SerializeField] ClickSpot clickSpot;
        [SerializeField] Vector2 hotSpot = Vector2.zero;
        [Header("Cursors")]
        [SerializeField] Texture2D walkCursor = null;
        [SerializeField] Texture2D enemyCursor = null;
        [SerializeField] Texture2D pickUpCursor = null;
        [SerializeField] Texture2D uiCursor = null;
        [SerializeField] Texture2D interactableCursor = null;
        [SerializeField] CursorMode mode = CursorMode.ForceSoftware;
        
        [Header("ClickSpot Objects")]
        [SerializeField] ClickSpotObj enemyClickSpotObj;
        [SerializeField] ClickSpotObj walkClickSpotObj;
        [SerializeField] ClickSpotObj abilityClickSpotObj;
        [SerializeField] ClickSpotObj uiClickSpotObj;
        [SerializeField] ClickSpotObj interactableClickSpotObj;
        //Add One For Interaction

        CameraRayCaster rayCaster;
        SelectionManager selectionManager;
        PlayerManagersHelper playerManagersHelper;

        ClickSpotObj objToFeed;
        Texture2D currCursorTexture;
        Player owner;
        private void Awake()
        {
            selectionManager = FindObjectOfType<SelectionManager>();
            rayCaster = GetComponent<CameraRayCaster>();
            playerManagersHelper = FindObjectOfType<PlayerManagersHelper>();
            playerManagersHelper.onOwnerSet += OnOwnerSet;
        }
        // Use this for initialization
        void Start()
        {
            rayCaster.OnMouseOverTerrainEvent += RayCaster_OnMouseOverTerrainEvent;
            rayCaster.OnMouseOverUnitEvent += RayCaster_OnMouseOverEnemyEvent;
            rayCaster.OnMouseOverUIEvent += RayCaster_OnMouseOverUIEvent;
        }

        public void OnOwnerSet(Player owner)
        {
            this.owner = owner;
        }

        private void RayCaster_OnMouseOverUIEvent(RaycastResult uiRayCastResult)
        {
            CheckAndChangeCursor(uiCursor);
            bool didClick = selectionManager.mouseManager.IsLeftDown() || selectionManager.mouseManager.IsRightDown();
            if (didClick)
            {
                objToFeed = uiClickSpotObj;
                ClickSpot clonedClickSpot = Instantiate(uiClickSpot, playerManagersHelper.TempUIElementsParent).GetComponentInChildren<ClickSpot>();

                Vector3 currCursorPos = uiRayCastResult.screenPosition;
                clonedClickSpot.GetComponent<RectTransform>().position = currCursorPos;
                clonedClickSpot.InitClickSpotAffect(objToFeed);
            }
        }
        private void RayCaster_OnMouseOverEnemyEvent(BaseUnit unit)
        {
            Unit u = unit as Unit;
            if (u && u.IsSamewOwner(owner))
            {
                CheckAndChangeCursor(walkCursor);
                CheckAndchangeObjToFeed(walkClickSpotObj);
            }
            else
            {
                CheckAndChangeCursor(enemyCursor);
                CheckAndchangeObjToFeed(enemyClickSpotObj);
            }
            if (selectionManager.mouseManager.IsRightDown() && selectionManager.HasSelections)
            {
                SpawnClickSpotAtPos(unit.transform.position);
            }
        }
        private void RayCaster_OnMouseOverTerrainEvent(Vector3 destination)
        {
            CheckAndChangeCursor(walkCursor);
            CheckAndchangeObjToFeed(walkClickSpotObj);

            if (selectionManager.mouseManager.IsRightDown() && selectionManager.HasSelections)
            {
                SpawnClickSpotAtPos(destination);
            }
        }
        private void SpawnClickSpotAtPos(Vector3 pos)
        {
            SpawnClickSpotObj(objToFeed, pos, Quaternion.identity);
        }
        private void SpawnClickSpotObj(ClickSpotObj obj,Vector3 pos, Quaternion rot = new Quaternion(), Transform parent = null)
        {
            if (parent==null)
            {
                parent = playerManagersHelper.TempObjsParent;
            }
           ClickSpot clonedClickSpot = Instantiate(clickSpot, pos, rot, parent);
            
            clonedClickSpot.InitClickSpotAffect(obj);
        }
        private void CheckAndChangeCursor(Texture2D newCursor)
        {
            if (currCursorTexture!=newCursor)
            {
                currCursorTexture = newCursor;
                Cursor.SetCursor(newCursor, hotSpot, mode);
            }
        }
        private void CheckAndchangeObjToFeed(ClickSpotObj newSpotObj)
        {
            if (objToFeed!=newSpotObj)
            {
                objToFeed = newSpotObj;
            }
        }
    }
}