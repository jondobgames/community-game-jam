﻿using UnityEngine;

namespace CMGJ.CameraAndUI
{
    [CreateAssetMenu(fileName = "Click Spot", menuName = "Stealth/Others/New Click Spot")]
    public class ClickSpotObj : ScriptableObject
    {
        public AnimationClip ClickSpotAnimation { get { return clickSpotAnimation; } }
        [SerializeField] AnimationClip clickSpotAnimation;
    }
}