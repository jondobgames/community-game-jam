﻿using System.Collections;
using UnityEngine;

namespace CMGJ.CameraAndUI
{
    public class ClickSpot : MonoBehaviour
    {
        const string rollingNameState = "Rolling";
        const string activate = "Activate";
        const string animToOverRide = "CP_DefRoll";
        AnimatorOverrideController overRidden;
        Animator animator;
        public void InitClickSpotAffect(ClickSpotObj obj)
        {
            animator = GetComponent<Animator>();
            overRidden = new AnimatorOverrideController( animator.runtimeAnimatorController);
            Activate(obj);
        }
        private void Activate(ClickSpotObj clickSpotObj)
        {
            if (clickSpotObj)
            {
                overRidden[animToOverRide] = clickSpotObj.ClickSpotAnimation;
                animator.runtimeAnimatorController = overRidden;
                animator.SetTrigger(activate);
                StartCoroutine(PlayAnimations());
            }
            else
            {
                Debug.LogWarning("No ClickSpotObj Was Fed, So The Affect Will Die");
                Destroy(gameObject);
            }
            
        }
        IEnumerator PlayAnimations()
        {
            yield return 0;
            while (animator.GetCurrentAnimatorStateInfo(0).IsName(rollingNameState) == true)
            {
                yield return 0;
            }
            Destroy(gameObject);
        }
    }
}