﻿using UnityEngine;
using UnityEngine.UI;
namespace CMGJ.CameraAndUI
{
    public class SelectionUI : MonoBehaviour
    {
        [SerializeField] Image selectionBox;
        Vector2 startPos;
        Vector2 endPos;
        Camera mainCam;
        public void UpdateSelectionBox()
        {
            endPos = Input.mousePosition;
            float dx = Mathf.Abs(endPos.x - startPos.x);
            float dy = Mathf.Abs(endPos.y - startPos.y);
            selectionBox.rectTransform.sizeDelta = new Vector2(dx, dy);
            selectionBox.rectTransform.position = (endPos + startPos) / 2.0f;
        }

        public void SetStartPosition()
        {
            startPos = Input.mousePosition;
        }
        private void Start()
        {
            mainCam = Camera.main;
            ReSetSelectionBox();
        }
        public void ReSetSelectionBox()
        {
            selectionBox.rectTransform.sizeDelta = Vector2.zero;
            selectionBox.rectTransform.position = Vector2.zero;
        }
        public void ReSetSelectionBox(out Ray startRay, out Ray endRay)
        {
            startRay = mainCam.ScreenPointToRay(startPos);
            endRay = mainCam.ScreenPointToRay(endPos);
            ReSetSelectionBox();
        }
    }
}