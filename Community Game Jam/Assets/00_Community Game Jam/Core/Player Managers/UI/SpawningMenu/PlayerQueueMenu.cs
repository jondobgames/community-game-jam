﻿using CMGJ.Units;
using System;
using UnityEngine;

namespace CMGJ.CameraAndUI
{
    public class SpawningMenu_Updating : Menu_Updating
    {
        public SpawningMenu_Updating(PlayerQueueMenu playerSpawningMenu) : base(playerSpawningMenu)
        {
        }
    }
    public class PlayerQueueMenu : PlayerMenu
    {
        public Sprite Transperancy { get { return transperancy; } }

        [SerializeField] Sprite transperancy;
        [SerializeField] SpawnQueue spawnQueue;
        SpawningMenu_Updating spawningMenuUpdating;
        PlayerCurrencyManager playerCurrencyManager;

        protected override void Start()
        {
            base.Start();
            spawningMenuUpdating = new SpawningMenu_Updating(this);
            spawnQueue.onQueueCompleted += SpawnQueue_onQueueCompleted;
            playerCurrencyManager = FindObjectOfType<PlayerCurrencyManager>();
        }
        public void Sub_OnDoneBuildingUnit(Action<UnitSpawnInfo> onDonebuilding)
        {
            spawnQueue.Sub_OnDoneBuildingUnit(onDonebuilding);
        }
        public void DeSub_OnDoneBuildingUnit(Action<UnitSpawnInfo> onDonebuilding)
        {
            spawnQueue.DeSub_OnDoneBuildingUnit(onDonebuilding);
        }
        private void SpawnQueue_onQueueCompleted()
        {
            TransitionToState(outro);
        }
        public void TryBuy(UnitSpawnInfo unitSpawnInfo)
        {
            TryQueue(unitSpawnInfo);
        }
        private void TryQueue(UnitSpawnInfo unitSpawnInfo)
        {
            if (playerCurrencyManager.CanAfford(unitSpawnInfo) && spawnQueue.Queue(unitSpawnInfo))
            {
                playerCurrencyManager.Buy(unitSpawnInfo);
                if (currState != UpdatingState())
                {
                    TransitionToState(entry);
                }
            }
        }
        public override Menu_Updating UpdatingState()
        {
            return spawningMenuUpdating;
        }
    }
}