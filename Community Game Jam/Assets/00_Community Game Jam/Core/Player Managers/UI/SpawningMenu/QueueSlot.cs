﻿using UnityEngine;
using UnityEngine.UI;
using CMGJ.Units;
namespace CMGJ.CameraAndUI
{
    public class QueueSlot : MonoBehaviour
    {
        public bool isFull { get { return unitSpawnInfo != null; } }
        public UnitSpawnInfo UnitSpawnInfo { get { return unitSpawnInfo; } }

        [SerializeField] Image icon;
        [SerializeField] Sprite originalIcon;

        private UnitSpawnInfo unitSpawnInfo;
        private void Start()
        {
            icon.sprite = originalIcon;
        }
        public virtual void EnQueue(UnitSpawnInfo unitSpawnInfo)
        {
            this.unitSpawnInfo = unitSpawnInfo;
            icon.sprite = unitSpawnInfo.Icon;
        }
        public virtual void DeQueue()
        {
            unitSpawnInfo = null;
            icon.sprite = originalIcon;
        }
    }
}