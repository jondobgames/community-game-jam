﻿using UnityEngine;
using UnityEngine.UI;
using CMGJ.Units;
using TMPro;
namespace CMGJ.CameraAndUI
{
    public class UnitSpawnButton : MonoBehaviour
    {
        [Header("Core Parts")]
        [SerializeField] UnitSpawnInfo unitSpawnInfo;
        [SerializeField] PlayerQueueMenu spawningMenu;
        [Header("Ui Parts")]
        [SerializeField] Image buttonImage;
        [SerializeField] TextMeshProUGUI costText;
        [SerializeField] ToggleEvent onFailedToSet;
        private void Reset()
        {
            spawningMenu = GetComponentInParent<PlayerQueueMenu>();
        }
        private void Start()
        {
            if (unitSpawnInfo == null)
            {
                Debug.LogWarning(name + "_ doesn't have a UnitSpawnInfo assigned to it");
                onFailedToSet.Invoke(false);
                return;
            }
            buttonImage.sprite = unitSpawnInfo.Icon;
            costText.text = unitSpawnInfo.Cost + "$";
        }
        public void CallSpawn()
        {
            spawningMenu.TryBuy(unitSpawnInfo);
        }
    }
}