﻿using CMGJ.Units;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace CMGJ.CameraAndUI
{
    public class BuildingSlot : QueueSlot
    {
        [SerializeField] Image timeRemaining;

        public event Action<UnitSpawnInfo> onDoneBuilding;
        public event Action onTakingNextSlot;
        float counter;
        Func<QueueSlot> nextSlot;

        public void SetUp(Func<QueueSlot> nextSlot)
        {
            this.nextSlot = nextSlot;
        }
        private void Update()
        {
            if (isFull == false)
            {
                return;
            }
            counter = counter + Time.deltaTime / UnitSpawnInfo.BuildTime;
            timeRemaining.fillAmount = 1 - counter;
            if (counter >= 1)
            {
                counter = 0;
                timeRemaining.fillAmount = 1;
                if (onDoneBuilding!=null)
                {
                    onDoneBuilding(UnitSpawnInfo);
                }
                DeQueue();
                StartNextProcess();
            }
        }
        private void TryTake(QueueSlot queueSlot)
        {
            if (isFull == false)
            {
                EnQueue(queueSlot.UnitSpawnInfo);
                queueSlot.DeQueue();
                if (onTakingNextSlot != null)
                {
                    onTakingNextSlot();
                }
            }
        }
        public void StartNextProcess()
        {
            if (nextSlot().isFull)
            {
                TryTake(nextSlot());
            }
        }
    }
}