﻿using CMGJ.Units;
using System;
using UnityEngine;

namespace CMGJ.CameraAndUI
{
    public class SpawnQueue : MonoBehaviour
    {
        public event Action onQueueCompleted;
        [SerializeField] BuildingSlot headSlot;
        [SerializeField] QueueSlot[] queueSlots;

        Func<QueueSlot> nextSlot;
        QueueSlot GetFirstEmpty()
        {
            foreach (QueueSlot qs in queueSlots)
            {
                if (qs.isFull == false)
                {
                    return qs;
                }
            }
            return null;
        }
        public bool Queue(UnitSpawnInfo unitSpawnInfo)
        {
            QueueSlot emptySlot = GetFirstEmpty();
            if (emptySlot != null)
            {
                emptySlot.EnQueue(unitSpawnInfo);
            }
            headSlot.StartNextProcess();
            return emptySlot != null;
        }
        void SortSlots()
        {
            for (int i = 0; i < queueSlots.Length - 1; i++)
            {
                if (queueSlots[i].isFull == false && queueSlots[i + 1].isFull)
                {
                    queueSlots[i].EnQueue(queueSlots[i + 1].UnitSpawnInfo);
                    queueSlots[i + 1].DeQueue();
                }
            }
        }
        void Start()
        {
            nextSlot = () => { return queueSlots[0]; };
            headSlot.SetUp(nextSlot);
            headSlot.onTakingNextSlot += SortSlots;
            headSlot.onDoneBuilding += HeadSlot_onDoneBuilding;
        }

        private void HeadSlot_onDoneBuilding(UnitSpawnInfo obj)
        {
            if (SlotsEmpty() && onQueueCompleted != null)
            {
                onQueueCompleted();
            }
        }
        public void Sub_OnDoneBuildingUnit(Action<UnitSpawnInfo> onDoneBuilding)
        {
            headSlot.onDoneBuilding += onDoneBuilding;
        }
        public void DeSub_OnDoneBuildingUnit(Action<UnitSpawnInfo> onDonebuilding)
        {
            headSlot.onDoneBuilding -= onDonebuilding;
        }
        bool SlotsEmpty()
        {
            foreach (QueueSlot qs in queueSlots)
            {
                if (qs.isFull)
                {
                    return false;
                }
            }
            return true;
        }
    }
}