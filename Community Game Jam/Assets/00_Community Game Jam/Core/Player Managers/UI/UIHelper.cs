﻿using UnityEngine;
using UnityEngine.UI;
namespace CMGJ
{
    public class UIHelper : MonoBehaviour
    {
        PlayerManager[] playerManagers = new PlayerManager[0];

        [SerializeField] bool useFullPlayerName = true;
        [SerializeField] float stopUpdatingAfter = 35.0f;
        [SerializeField] Text helperText;

        [SerializeField] float timeBetweenChecks = 1;

        public void Quit()
        {
            Application.Quit();
        }
        float updatingCounter;
        float counter;
        private void Awake()
        {
            if (helperText==null)
            {
                helperText = GetComponentInChildren<Text>();
            }
        }
        private void Update()
        {
            if (updatingCounter<1)
            {
                updatingCounter += Time.deltaTime / stopUpdatingAfter;
            }
            if (updatingCounter>=1)
            {
                return;
            }
            string text = string.Empty;
            foreach (PlayerManager pm in playerManagers)
            {
                string pName = string.Empty;
                if (useFullPlayerName)
                {
                    pName = pm.player.FullPlayerName;
                }
                else
                {
                    pName = pm.player.playerName;
                }
                text += pName + "\n";
            }
            helperText.text = text;
            
            counter += Time.deltaTime / timeBetweenChecks;
            if (counter<1)
            {
                return;
            }
            if (counter>=1||counter<0)
            {
                counter = 0;
            }
            playerManagers = FindObjectsOfType<PlayerManager>();
        }
    }
}