﻿using UnityEngine;
using CMGJ.Units;

namespace CMGJ.CameraAndUI
{
    public class PlayerUIManager : PlayerMenu
    {
        [SerializeField] bool forceReso = true;
        [SerializeField] Vector2Int reso = new Vector2Int(1920, 1080);
        [SerializeField] PlayerHudUI playerHudUI;
        [SerializeField] PlayerQueueMenu playerSpawningMenu;

        Vector2Int oriReso;
        bool oriFullScreen;
        public void DisplayUnitInformation(UnitInfo unitInfo, float currHealth, float totalHealth)
        {
            playerHudUI.Display(unitInfo, currHealth, totalHealth);
        }
        
        protected override void Start()
        {
            base.Start();
            Resolution currReso = Screen.currentResolution;

            oriReso = new Vector2Int();
            oriReso.x = currReso.width;
            oriReso.y = currReso.height;
            oriFullScreen = Screen.fullScreen;

            if (forceReso)
            {
                Screen.SetResolution(reso.x, reso.y, true);
            }
            TransitionToState(entry);
        }
        private void OnDisable()
        {
            Screen.SetResolution(oriReso.x, oriReso.y, oriFullScreen);
        }
        public void Exit()
        {
            TransitionToState(outro);
        }
    }
}