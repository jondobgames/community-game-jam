﻿using Prototype.NetworkLobby;
using UnityEngine;
using UnityEngine.Networking;

namespace CMGJ
{
    public class RTS_LobbyManager : LobbyManager
    {
        [Header("Core Prefabs")]
        [SerializeField] GameObject[] corePrefabs;
        [Header("Units Prefabs")]
        [SerializeField] GameObject[] unitsPrefabs;
        [Header("Units Prefabs")]
        [SerializeField] GameObject[] effectsPrefabs;

        public override void OnClientConnect(NetworkConnection conn)
        {
            RegiesterObjs(corePrefabs);
            RegiesterObjs(unitsPrefabs);
            RegiesterObjs(effectsPrefabs);

            base.OnClientConnect(conn);
        }
        void RegiesterObjs(GameObject[] objs)
        {
            foreach (GameObject o in objs)
            {
                ClientScene.RegisterPrefab(o);
            }
        }
    }
}