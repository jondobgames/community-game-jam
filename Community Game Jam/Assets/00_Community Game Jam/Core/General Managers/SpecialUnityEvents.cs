﻿using System;
using UnityEngine.Events;

namespace CMGJ
{
    [Serializable] public class OwnerSetEvent : UnityEvent<Player> { }
    [Serializable] public class ToggleEvent : UnityEvent<bool> { }
    [Serializable] public class FloatEvent : UnityEvent<float> { }
}