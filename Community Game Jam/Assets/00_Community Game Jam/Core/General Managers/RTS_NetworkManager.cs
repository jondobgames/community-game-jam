﻿using System;
using UnityEngine;
using UnityEngine.Networking;
namespace CMGJ
{
    public class RTS_NetworkManager : NetworkManager
    {
        [Header("Core Prefabs")]
        [SerializeField] GameObject[] corePrefabs;
        [Header("Units Prefabs")]
        [SerializeField] GameObject[] unitsPrefabs;
        [Header("Units Prefabs")]
        [SerializeField] GameObject[] effectsPrefabs;
        public override void OnStartClient(NetworkClient client)
        {
            base.OnStartClient(client);
        }
        public override void OnClientConnect(NetworkConnection conn)
        {
            RegiesterObjs(corePrefabs);
            RegiesterObjs(unitsPrefabs);
            RegiesterObjs(effectsPrefabs);
            base.OnClientConnect(conn);
        }
        public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
        {
            base.OnServerAddPlayer(conn, playerControllerId);
        }
        void RegiesterObjs(GameObject[] objs)
        {
            foreach (GameObject o in objs)
            {
                ClientScene.RegisterPrefab(o);
            }
        }
    }
}