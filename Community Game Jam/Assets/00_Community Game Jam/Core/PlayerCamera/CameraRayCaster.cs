﻿using UnityEngine;
using CMGJ.Units;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace CMGJ.CameraAndUI
{
    public class CameraRayCaster : MonoBehaviour
    {
        public delegate void OnMouseOverTerrainDelegate(Vector3 position);
        public event OnMouseOverTerrainDelegate OnMouseOverTerrainEvent;
        public delegate void OnMouseOverUnitDelegate(BaseUnit unit);
        public event OnMouseOverUnitDelegate OnMouseOverUnitEvent;
        public delegate void OnMouseOverUI(RaycastResult uiRayCastResult);
        public event OnMouseOverUI OnMouseOverUIEvent;

        [SerializeField] Camera mainCam;
        [SerializeField] float maxCastDist = 200;
        [SerializeField] float boxSelectionHegith = 0.1f;

        ExtendedInputModule extendedInputModule;
        int terrainMask = 1 << Constants.TerrainLayer;
        private void Start()
        {
            extendedInputModule = FindObjectOfType<ExtendedInputModule>();
        }
        private void Update()
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                RayCastForUI();
                return;
            }
            Ray ray = mainCam.ScreenPointToRay(Input.mousePosition);
            RaycastHit whatWashit;
            if (RayCastForUnit(ray, out whatWashit))
            {
                return;
            }
            if (RayCastForTerrain(ray, out whatWashit))
            {
                return;
            }
        }
        private void RayCastForUI()
        {
            PointerEventData pEventData = extendedInputModule.GetPointerEventData();
            RaycastResult uiRayCastResult = pEventData.pointerCurrentRaycast;
            if (OnMouseOverUIEvent != null)
            {
                OnMouseOverUIEvent(uiRayCastResult);
            }
        }
        private bool RayCastForUnit(Ray ray, out RaycastHit whatWasHit)
        {
            bool didHit = Physics.Raycast(ray, out whatWasHit, maxCastDist);
            Selectivity selectivity = null;
            if (didHit)
            {
                selectivity = whatWasHit.collider.GetComponent<Selectivity>();
                if (selectivity != null && OnMouseOverUnitEvent != null)
                {
                    OnMouseOverUnitEvent(selectivity.Unit);
                }
            }
            return selectivity != null;
        }
        public bool RayCastOnTerrain(Ray ray, out RaycastHit whatWasHit)
        {
            return RayCastForTerrain(ray, out whatWasHit, false);
        }
        private bool RayCastForTerrain(Ray ray, out RaycastHit whatWasHit, bool callMouseOverTerrainEvent = true)
        {
            bool didHit = Physics.Raycast(ray, out whatWasHit, maxCastDist, terrainMask);
            if (didHit && callMouseOverTerrainEvent)
            {
                if (OnMouseOverTerrainEvent != null)
                {
                    OnMouseOverTerrainEvent(whatWasHit.point);
                }
            }
            return didHit;
        }
        public List<BaseUnit> BoxCastForUnits(Ray startRay, Ray endRay)
        {
            RaycastHit startHit, endHit;
            bool didHitStart = RayCastForTerrain(startRay, out startHit, false);
            bool didHitEnd = RayCastForTerrain(endRay, out endHit, false);
            List<BaseUnit> unitsInBox = new List<BaseUnit>();

            if (didHitEnd && didHitStart)
            {
                Vector3 start = startHit.point;
                Vector3 end = endHit.point;
                Vector3 center = (start + end) / 2.0f;
                Vector3 extents = (end - start) / 2.0f;
                extents.x = Mathf.Abs(extents.x);
                extents.y = boxSelectionHegith;
                extents.z = Mathf.Abs(extents.z);

                RaycastHit[] hits = Physics.BoxCastAll(center, extents, Vector3.up, Quaternion.identity, maxCastDist);

                if (hits.Length != 0)
                {
                    foreach (RaycastHit hit in hits)
                    {
                        if (hit.collider != null)
                        {
                            Selectivity selectivity = hit.collider.GetComponent<Selectivity>();
                            if (selectivity)
                            {
                                unitsInBox.Add(selectivity.Unit);
                            }
                        }
                    }
                }
            }
            return unitsInBox;
        }
    }
}