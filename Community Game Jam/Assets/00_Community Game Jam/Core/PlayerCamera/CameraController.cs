﻿using UnityEngine;

namespace CMGJ.CameraAndUI
{
    public class CameraController : MonoBehaviour
    {
        [Header("Keys")]
        [SerializeField] KeyCode lookAtCoreKey = KeyCode.F;
        [SerializeField] KeyCode restZoomKey= KeyCode.R;
        [SerializeField] KeyCode autoElevation = KeyCode.A;

        [Header("Zoom")]
        [SerializeField] float minZoom = 20;
        [SerializeField] float maxZoom = 60;
        [SerializeField] float defaultZoom = 40;
        [SerializeField] float zoomSpeed = 50;

        [Header("Movement")]
        [SerializeField] bool useVelocity;
        [SerializeField] float movementSpeed = 20;
        [SerializeField] float movementSkin = 0.1f;
        [SerializeField] float catchUpThreshold = 0.250f;
        [SerializeField] Vector3 offSet = new Vector3(0.5f, 22, -12.5f);
        
        Camera mainCam;
        Vector3 movementDir = new Vector3();
        Transform initialLookTarget;
        Transform camTransform;
        Rigidbody rb3d;
        Vector2 mousePos;
        CameraRayCaster rayCaster;
        float optimalHegith;
        bool canAutoElevate = false;

        MouseManager mm = new MouseManager();
        public CameraRayCaster RayCaster()
        {
            if (rayCaster == null)
            {
                rayCaster = GetComponent<CameraRayCaster>();
            }
            return rayCaster;
        }
        private void Start()
        {
            mainCam = Camera.main;
            camTransform = mainCam.transform;
            rb3d = camTransform.GetComponent<Rigidbody>();
            optimalHegith = offSet.y;
            rayCaster = GetComponent<CameraRayCaster>();
            mainCam.fieldOfView = defaultZoom;
        }
        private void Update()
        {
            if (mm.IsLeftHeld())
            {
                return;
            }
            SetMousePosition();
            DetermineDirection();
            if (canAutoElevate)
            {
                DetectDown();
            }

            Move();
            Zoom();

            if (IsKeyUp(lookAtCoreKey))
            {
                SetUp(initialLookTarget);
            }
            if (IsKeyUp(restZoomKey))
            {
                ReSetZoom();
            }
            if (IsKeyUp(autoElevation))
            {
                SwitchAutoElevation();
            }
        }

        private void Zoom()
        {
            if (canAutoElevate)
            {
                return;
            }
            float scroll = Input.GetAxis("Mouse ScrollWheel") * -1;
            if (scroll==0)
            {
                return;
            }

            float zoomDir = Mathf.Sign(scroll);
            float targetZoom = mainCam.fieldOfView + zoomSpeed * zoomDir * Time.deltaTime;

            if (targetZoom>maxZoom||targetZoom<minZoom)
            {
                return;
            }
            mainCam.fieldOfView = Mathf.MoveTowards(mainCam.fieldOfView, targetZoom, zoomSpeed * Time.deltaTime);
        }

        private void ReSetZoom()
        {
            mainCam.fieldOfView = defaultZoom;
        }
        private void SwitchAutoElevation()
        {
            canAutoElevate = !canAutoElevate;
        }
        public bool IsKeyUp(KeyCode k)
        {
            return Input.GetKeyUp(k);
        }

        private void DetectDown()
        {
            Ray ray = new Ray(camTransform.position, Vector3.down);
            RaycastHit whatWasHit;
            if (rayCaster.RayCastOnTerrain(ray, out whatWasHit))
            {
                float currentHegith = camTransform.position.y - whatWasHit.point.y;
                float difference = optimalHegith - currentHegith;
                if (Mathf.Abs(difference) > catchUpThreshold)
                {
                    movementDir.y = Mathf.Sign(difference);
                }
            }
        }

        private void SetMousePosition()
        {
            mousePos = Input.mousePosition;
            mousePos = mainCam.ScreenToViewportPoint(mousePos);
            mousePos = mousePos + Vector2.one * -0.5f;
        }
        private void DetermineDirection()
        {
            movementDir.Set(0, 0, 0);
            bool canMove=false;
            if (CanMove(mousePos.x))
            {
                canMove = true;
                movementDir.x = Mathf.Sign(mousePos.x);
            }
            if (CanMove(mousePos.y))
            {
                canMove = true;
                movementDir.z = Mathf.Sign(mousePos.y);
            }

            if (canMove)
            {
                movementDir = camTransform.TransformDirection(movementDir);
                movementDir.y = 0;
                if (movementDir.x!=0)
                {
                    movementDir.x = Mathf.Sign(movementDir.x);
                }
                if (movementDir.z!=0)
                {
                    movementDir.z = Mathf.Sign(movementDir.z);
                }
            }
        }
        private void Move()
        {
            if (useVelocity)
            {
                rb3d.velocity = movementSpeed * movementDir;
                return;
            }
            Vector3 targetPos = camTransform.position + movementDir * movementSpeed * Time.deltaTime;
            camTransform.position
                = Vector3.MoveTowards(camTransform.position, targetPos, movementSpeed * Time.deltaTime);
        }

        private bool CanMove(float on)
        {
            float delta = 0.5f - Mathf.Abs(on);
            return delta <= movementSkin;
        }

        public void SetUp(Transform initialLookTarget)
        {
            if (initialLookTarget==null)
            {
                return;
            }
            camTransform.position = initialLookTarget.position + offSet;
            this.initialLookTarget = initialLookTarget;
        }
    }
}